import unittest

from general import jogadas


class TestJogadas(unittest.TestCase):
    def test_trincas(self):
        for (mao, pontuacao) in [([1,1,1,2,3], 8),
                                 ([1,3,3,3,6], 16),
                                 ([2,4,6,6,6], 24),
                                 ([1,2,3,4,5], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.trinca(mao), pontuacao)

    def test_quadras(self):
        for (mao, pontuacao) in [([1,1,1,1,2], 6),
                                 ([1,6,6,6,6], 25),
                                 ([2,4,6,6,6], 0),
                                 ([1,2,3,4,5], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.quadra(mao), pontuacao)

    def test_fullhouse(self):
        for (mao, pontuacao) in [([1,1,1,2,2], 25),
                                 ([1,1,2,2,2], 25),
                                 ([2,4,6,6,6], 0),
                                 ([1,2,3,4,5], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.full_house(mao), pontuacao)

    def test_seqalta(self):
        for (mao, pontuacao) in [([2,3,4,5,6], 30),
                                 ([1,1,2,2,2], 0),
                                 ([2,4,6,6,6], 0),
                                 ([1,2,3,4,5], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.seq_alta(mao), pontuacao)

    def test_seqbaixa(self):
        for (mao, pontuacao) in [([1,2,3,4,5], 40),
                                 ([1,1,2,2,2], 0),
                                 ([2,4,6,6,6], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.seq_baixa(mao), pontuacao)

    def test_general(self):
        for (mao, pontuacao) in [([1,1,1,1,1], 50),
                                 ([2,2,2,2,2], 50),
                                 ([3,3,3,3,3], 50),
                                 ([4,4,4,4,4], 50),
                                 ([5,5,5,5,5], 50),
                                 ([1,3,3,3,3], 0)]:
            with self.subTest(mao=mao, pontuacao=pontuacao):
                self.assertEqual(jogadas.general(mao), pontuacao)

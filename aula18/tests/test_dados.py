import unittest

from general import dados


class TestDados(unittest.TestCase):
    def test_rolamento(self):
        for i in range(1, 6):
            with self.subTest(rolamentos=i):
                self.assertEqual(len(dados.rolar_dados(i)), i)

    def test_rolar_mais_que_permitido(self):
        with self.assertRaises(ValueError):
            dados.rolar_dados(6)

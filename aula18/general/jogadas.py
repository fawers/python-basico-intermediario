def trinca(dados_ord: list[int]):
    match dados_ord:
        case [a, b, c, d, e] if a == b == c or b == c == d  or c == d == e:
            return sum(dados_ord)

    return 0


def quadra(dados_ord):
    match dados_ord:
        case [a, b, c, d, e] if a == b == c == d or b == c == d == e:
            return sum(dados_ord)

    return 0


def full_house(dados_ord):
    match dados_ord:
        case [a, b, c, d, e] if a == b == c and d == e or a == b and c == d == e:
            return 25

    return 0


def seq_alta(dados_ord):
    match dados_ord:
        case [2, 3, 4, 5, 6]:
            return 30

    return 0


def seq_baixa(dados_ord):
    return 40 if dados_ord == [1, 2, 3, 4, 5] else 0


def general(dados_ord):
    if dados_ord.count(dados_ord[0]) == 5:
        return 50

    return 0


def jogadas_possiveis(dados: list[int]) -> dict[str, int]:
    dados_ord = sorted(dados)

    return {
        '1': dados.count(1),
        '2': dados.count(2) * 2,
        '3': dados.count(3) * 3,
        '4': dados.count(4) * 4,
        '5': dados.count(5) * 5,
        '6': dados.count(6) * 6,
        'T': trinca(dados_ord),
        'Q': quadra(dados_ord),
        'F': full_house(dados_ord),
        'S+': seq_alta(dados_ord),
        'S-': seq_baixa(dados_ord),
        'G': general(dados_ord),
        'X': sum(dados)
    }

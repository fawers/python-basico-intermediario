class Jogador:
    def __init__(self, identificador: int):
        self.id = identificador
        self.cartela = {'1': -1, '2': -1, '3': -1, '4': -1, '5': -1, '6': -1,
                        'T': -1, 'Q': -1, 'F': -1, 'S+': -1, 'S-': -1, 'G': -1, 'X': -1}

    def jogadas_feitas(self):
        jfs = {}

        for (jogada, pontos) in self.cartela.items():
            if pontos != -1:
                jfs[jogada] = pontos

        return jfs

    def esconder_feitas(self, jogadas_possiveis: dict[str, int]):
        jfs = self.jogadas_feitas()

        for jogada in jfs:
            jogadas_possiveis.pop(jogada)

    def pontuacao_final(self) -> int:
        pontos = 0

        for pontuacao in self.jogadas_feitas().values():
            pontos += pontuacao

        return pontos

import random
from datetime import datetime


R = random.Random(datetime.now().timestamp())


def rolar_dados(num_rolamentos=5):
    if num_rolamentos > 5:
        raise ValueError("num_rolamentos > 5")

    rolamentos = []

    for _ in range(num_rolamentos):
        rolamentos.append(R.randint(1, 6))

    return rolamentos

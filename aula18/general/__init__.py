from . import dados
from .jogadores import Jogador
from .jogadas import jogadas_possiveis


class Jogo:
    def __init__(self, num_jogadores: int):
        if num_jogadores < 2:
            raise ValueError("deve haver pelo menos 2 jogadores")

        self.jogadores = []

        for i in range(num_jogadores):
            self.jogadores.append(Jogador(i))

    def instrucoes(self):
        pass

    def comecar(self):
        for rodada in range(1, 2):
            print(f"Iniciando {rodada}ª rodada.")

            for jogador in self.jogadores:
                print()
                rolagens = 0
                ds = []
                jps = {}

                while True:
                    comando = input(f"Jogador {jogador.id}, o que você deseja fazer? ").split()

                    match comando:
                        case ["rolar", *numeros] if rolagens < 3:
                            if len(numeros) == 0:
                                rolagens += 1
                                ds = dados.rolar_dados()

                            else:
                                try:
                                    for n in numeros:
                                        ds.remove(int(n))
                                    ds.extend(dados.rolar_dados(len(numeros)))
                                    rolagens += 1

                                except ValueError:
                                    print("Escolha outros dados para rolar novamente.")
                                    continue

                            jps = jogadas_possiveis(ds)
                            jogador.esconder_feitas(jps)

                            print("Jogadas possíveis:")
                            for (jogada, pontos) in jps.items():
                                print(f"{jogada} (+{pontos})")
                            print(f"dados = {ds}")

                        case ["escolher", _] if len(jps) == 0:
                            print("Você ainda não rolou dados.")

                        case ["escolher", ('1'|'2'|'3'|'4'|'5'|'6'|'T'|'Q'|'F'|'S+'|'S-'|'G'|'X') as jogada]:
                            if jogador.cartela[jogada] == -1:  # jogada não preenchida
                                jogador.cartela[jogada] = jps[jogada]
                                print(jogador.jogadas_feitas())
                                break

                            else:  # jogada preenchida
                                print("Você não pode escolher novamente esta jogada.")

                        case _:
                            print("Não entendi o que você quer.")

        for j in self.jogadores:
            print(f"Jogador {j.id}: {j.pontuacao_final()} pontos")
            print(j.jogadas_feitas())

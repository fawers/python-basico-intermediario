# pattern matching, casamento de padrões

# print(f"{=}")


def exemplo1():
    t = ('Fabricio', 30)
    (nome, idade) = t
    print(f"{t=}")
    print(f"{nome=}, {idade=}")

    name, age = 'Fabricio', 30


def exemplo2():
    l = [10, 20, 30, 40, 50]
    [x, y, *_, z] = l
    print(f"{x=}, {y=}, {z=}")


def exemplo3():
    d = [('curso', ('Python', None)),
         ('duracao', (17, 18)),
         ('professor', ('Fabricio', 'internet'))]  # d.keys(), d :: dict

    for (k, (v, v2)) in d:
        print(f"{k=}, {v=}, {v2=}")


def exemplo4():
    d = {'curso': 'Python', 'professor': 'Fabricio', 'aulas': [1, 2, 3], 'duracao': (17, 18)}

    match d:
        case {'duracao': (menor, maior)}:
            print(f"{menor=}, {maior=}")

        case {'aulas': [x]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulas']) == 1:
            #   x = d['aulas'][0]
            print(f"{x=}")

        case {'aulas': [x, *_]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulas']) >= 1:
            #   x = d['aulas'][0]
            print(f"{x=}")

        case {'aulas': [*_, x]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulas']) >= 1:
            #   x = d['aulas'][-1]
            print(f"{x=}")

        case {'aulas': [*_, x, _]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulas']) >= 2:
            #   x = d['aulas'][-2]
            print(f"{x=}")

        case {'aulas': [_, _, _, x]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulass']) == 4:
            #   x = d['aulas'][3]
            print(f"{x=}")

        case {'aulas': [_, _, _, x, *_]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulass']) >= 4:
            #   x = d['aulas'][3]
            print(f"{x=}")

        case {'aulas': [_, x, *_]}:
            # linha acima realiza as seguintes checagens:
            # if 'aulas' in d and isinstance(d['aulas'], list) and len(d['aulas']) >= 2:
            #   x = d['aulas'][1]
            print(f"{x=}")

        case {'professor': p}:
            # linha acima é realiza as seguintes checagens:
            # if 'professor' in d:
            #     p = d['professor']
            print(f"{p=}")

    print("fim do match")


def exemplo5():
    d = {'artista': 'No Mana', 'albuns': {'1up': {'musicas': ['Array of Sirens', 'Redux']}}}

    match d:
        case {'albuns': {'1up': {'musicas': ms}}}:
            # checagens:
            #     'albuns' in d and isinstance(d['albuns'], dict)
            # and '1up' in d['albuns'] and isinstance(d['albuns']['1up'], dict)
            # and 'musicas' in d['albuns']['1up']
            print(f"{ms=}")


if __name__ == '__main__':
    exemplo5()

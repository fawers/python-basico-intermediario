def soma_nums(nums: list[int]) -> int:
    s = 0

    for n in nums:
        s += n

    return s


def soma_nums_rec(nums: list[int]) -> int:
    if len(nums) == 0:
        return 0

    else:
        head = nums[0]
        tail = nums[1:]
        soma_tail = soma_nums_rec(tail)
        return head + soma_tail


def soma_nums_rec_patmat(nums: list[int]) -> int:
    match nums:
        case []:
            return 0

        case [x, *xs]:
            soma_xs = soma_nums_rec_patmat(xs)
            return x + soma_xs


def main():
    print(soma_nums_rec_patmat([10, 15, 25]))


if __name__ == '__main__':
    main()

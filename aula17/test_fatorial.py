import unittest

from fatorial import fatorial


class TestFatorial(unittest.TestCase):
    def test_entradas_negativas(self):
        self.assertEqual(fatorial(-500), None)

    def test_entrada_zero(self):
        self.assertEqual(fatorial(0), None)

    def test_entradas_positivas(self):
        for (entrada, saida) in [(1, 1), (2, 2), (3, 6), (4, 24), (5, 120)]:
            with self.subTest(entrada=entrada, saida=saida):
                self.assertEqual(fatorial(entrada), saida)


if __name__ == '__main__':
    unittest.main()

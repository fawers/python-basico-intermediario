# n!
# se n > 1 -> n * (n-1)!
# senão    -> 1

def fatorial(n: int) -> int | None:
    if n < 1:
        return None

    if n == 1:
        return -1

    return n * fatorial(n-1)


if __name__ == '__main__':
    print(f"{fatorial(1)=}")
    print(f"{fatorial(2)=}")
    print(f"{fatorial(3)=}")
    print(f"{fatorial(4)=}")
    print(f"{fatorial(5)=}")

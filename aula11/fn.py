def main():
    fn_args()
    fn_args("um")
    fn_args(1, 2)
    fn_args("primeiro", 2, True, None, 5.0)


# *args: pode passar quantos argumentos posicionais quiser ou precisar.
def fn_args(*args):
    print(args)


def main2():
    fn_kwargs()
    fn_kwargs(primeiro=2)
    fn_kwargs(segunda_chamada=False, terceira_chamada=True)


# **kwargs: pode passar quantos argumentos chave:valor quiser ou precisar.
def fn_kwargs(**kwargs):
    #           ^^ keyword  key:value chave:valor
    print(kwargs)


def main3():
    fn_args_kwargs(1, 2, 3, 6, quarto=4, quinto=5, sexto=7)


# *args e **kwargs: pode usar os dois, mas somente nesta ordem.
def fn_args_kwargs(*args, **kwargs):
    print(f"{args=}, {kwargs=}")


def main4():
    fn_barra(1, 2, 3)
    fn_estrela(1, 2, arg3=3)


# arg1, arg2, /, arg3:
# os argumentos à esquerda da barra são todos posicionais. não
# é possível passar como chave:valor.
# arg3 pode ser passado posicionalmente ou como chave:valor.
def fn_barra(arg1, arg2, /, arg3):
    print(f"{arg1=}, {arg2=}, {arg3=}")


# arg1, arg2, *, arg3:
# os argumentos à direita da estrela são todos chave:valor.
# arg3 pode somente ser passado da seguinte forma: arg3=argumento.
# arg1 e arg2 podem ser passados posicionalmente ou como chave:valor.
def fn_estrela(arg1, arg2, *, arg3):
    print(f"{arg1=}, {arg2=}, {arg3=}")


def main5():
    fn_tudao(1, 2, 3, 4, 5, 6, 7, 8, arg3=9, arg4=10, arg5=11)


# * e / podem ser usados ao mesmo tempo, mas precisa ser nessa ordem.
# *args e **kwargs também podem ser usados simultaneamente.
# depois de *args, TODOS os parâmetros devem ser passados por chave:valor.
def fn_tudao(arg1, arg2, *args, arg3, **kwargs):
    print(locals())


if __name__ == '__main__':
    main5()

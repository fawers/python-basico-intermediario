def main():
    print(f"{'Fabricio'.find('bric')=}")
    print(f"{'Fabricio'.find('oicir')=}")
    print(f"{'Fabricio'.casefold()=}")
    print(f"{'Fabricio'=:@^10}")
    print(f"{'Fabricio'.count('i')=}")
    print(f"{'Fabricio'.count('F')=}")
    print(f"{'Fabricio'.count('u')=}")
    print(f"{'Fabricio'.encode()=}")
    print(f"{b'Fabricio'.decode()=}")
    print(f"{'fAbRiCiO da silva werneck'.capitalize()=}")
    print(f"{'fAbRiCiO'.upper()=}")
    print(f"{'fAbRiCiO'.lower()=}")
    print(f"{'fAbRiCiO da silva werneck'.title()=}")
    print(f"{'Fabricio'.index('bric')=}")
    # print(f"{'Fabricio'.index('oicir')=}")  # ValueError!
    print(f"{'Fabricio'.isalnum()=}")
    print(f"{'Fabricio'.isalpha()=}")
    print(f"{'Fabricio'.isdigit()=}")
    print(f"{'Fabricio'.isascii()=}")  # tabela ASCII
    print(f"{'Fabricio'.islower()=}")
    print(f"{'Fabricio'.isupper()=}")
    print(f"{'Fabricio'.replace('ricio', 'io')=}")

    print(f"{' '.join(['Fabricio', 'Werneck'])=}")
    print(f"{'Fabricio Werneck'.split(' ')=}")

    print(f"{'Fabricio'.ljust(10)= }")
    print(f"{'Fabricio'.center(10)=}")
    print(f"{'Fabricio'.rjust(10)= }")

    print(f"{'Fabricio'.startswith('Fab')=}")
    print(f"{'Fabricio'.startswith('io')=}")
    print(f"{'Fabricio'.endswith('io')=}")
    print(f"{'Fabricio'.endswith('iO')=}")

    print(f"{'modo=fsck=force'.partition('=')=}")
    print(f"{'modo=fsck=force'.rpartition('=')=}")

    print(f"{'   Fabricio   '.strip()=}")
    print(f"{'   Fabricio   '.lstrip()=}")
    print(f"{'   Fabricio   '.rstrip()=}")

    print(f"{'Fabricio'[2:]=}")
    print(f"{'Fabricio'[:3]=}")
    print(f"{'Fabricio'[6:2:-1]=}")


if __name__ == '__main__':
    main()

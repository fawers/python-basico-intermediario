def paginacao(total_paginas: int, tamanho_barra: int, cursor: int) -> tuple[int, int, int]:
    # 1 <= cursor <= total_paginas
    # tamanho_barra > total_paginas -> tamanho_barra = total_paginas

    if not (1 <= cursor <= total_paginas):
        raise ValueError({
            True: f"{cursor < 1 = }",
            False: f"{cursor > total_paginas = }"}[cursor < 1])

    if tamanho_barra > total_paginas:
        tamanho_barra = total_paginas

    # [1 2 3 4 5 6 7 8 9 10]
    # 3 - 5 = -2 -> 1
    # 3 + 5 = 8 - 1 = 7 -> 3

    # 8 - 5 = 3 -> 1
    # 8 + 5 - 1 = 12 -> 10


    # [1 2 3 4 5 6]

    meio = tamanho_barra // 2
    primeira, ultima = cursor - meio, cursor + meio

    if tamanho_barra % 2 == 0:
        ultima -= 1

    if primeira < 1:
        dif = 1 - primeira
        primeira += dif
        ultima += dif

    elif ultima > total_paginas:
        dif = ultima - total_paginas
        ultima -= dif
        primeira -= dif

    return primeira, ultima, cursor


def barra_paginacao(prim_pag: int, ult_pag: int, cursor: int) -> str:
    barra: list[int] = list(range(prim_pag, ult_pag + 1))
    barra2 = []

    for pagina in barra:
        if pagina == cursor:
            barra2.append(f"\x1b[41m{pagina:-2d}\x1b[0m")

        else:
            barra2.append(f"{pagina:-2d}")

    return ' '.join(barra2)


def main():
    tam, tot = 10, 15
    for cur in range(1, tot+1):
        dados_pag = paginacao(tot, tam, cur)  # -> (3, 12, 8)
        print(barra_paginacao(*dados_pag))


if __name__ == '__main__':
    main()

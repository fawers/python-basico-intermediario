# Calcular se um ano de entrada é bissexto ou não.
# Exibir na tela: True ou False

# Um ano é bissexto se
# - ele for divisível por 400
# OU
# - ele for divisível por 4
# E
# - não for divisível por 100

# divisível por 400 ou divisível por 4 mas não por 100

ano = int(input("Digite um ano: "))

# 2000 True
# 2016 True
# 2022 False
# 3000 False
print(False)

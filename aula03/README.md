# aula 3

O exercício se encontra nos arquivo `ex.py`.

Para verificar se a sua solução está correta, execute o arquivo `test.py`: este validará se a saída
do seu programa está de acordo com o combinado na aula gravada.

import io
import sys
import unittest
from importlib import reload
from unittest.mock import patch


class TestLeapYear(unittest.TestCase):
    STDIN = sys.stdin

    def _import_module(self):
        if 'ex' in sys.modules:
            reload(sys.modules['ex'])

        else:
            import ex

    @classmethod
    def tearDownClass(cls):
        sys.stdin = cls.STDIN

    def test_years(self):
        for params in [{'year': '2000\n', 'expected': 'True', 'verb': 'é'},
                       {'year': '2016\n', 'expected': 'True', 'verb': 'é'},
                       {'year': '2022\n', 'expected': 'False', 'verb': 'não é'},
                       {'year': '3000\n', 'expected': 'False', 'verb': 'não é'}]:
            with self.subTest(**params):
                sys.stdin = io.StringIO(params['year'])

                with patch('sys.stdout', io.StringIO()) as output:
                    self._import_module()
                    output.seek(0)
                    msg = output.read().strip()
                    self.assertTrue(msg.endswith(params['expected']), f'O ano {params["year"].strip()} {params["verb"]} bissexto.')


if __name__ == '__main__':
    unittest.main()

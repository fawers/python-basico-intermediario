import unittest

import add


class TestAdd(unittest.TestCase):
    def test_add_adds_two_numbers(self):
        a, b = 3, 7

        expected = a + b
        actual = add.add(a, b)

        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()

age = 13

# se a idade (age) for maior do que 18
if age >= 18:
    # então print
    print("Você já tem idade para dirigir!")

# else if
# senão se
elif age == 17:
    print("Falta um aninho para você poder tirar sua carta!")

else:  # caso contrário
    print(f"Você ainda precisa esperar {18 - age} anos para poder dirigir.")
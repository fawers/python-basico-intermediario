import csv
import json

def test_json():
    arquivo = open("contador.json", 'r')  # read / leitura (implícito se não incluído)
    contador = json.load(arquivo)
    arquivo.close()

    print(contador)
    contador['contador'] += 1

    if not contador['iniciado']:
        contador['iniciado'] = True

    arquivo = open('contador.json', 'w')  # write / escrita
    json.dump(contador, arquivo)
    arquivo.close()

# test_json()

def test_csv():
    arquivo = open('tabela.csv', 'a')  # append / escrita ao final
    writer = csv.writer(arquivo, delimiter=';')
    writer.writerow(['xpto', '40'])
    arquivo.close()

test_csv()

def main():
    t = (1, 2, 3)
    print(t)
    print(t[1])
    # t[0] = 4
    print(id(t))
    t += (4,)
    print(t)
    print(id(t))


if __name__ == '__main__':
    main()

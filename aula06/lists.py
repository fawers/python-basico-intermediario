def main():
    a = [1, 2, 3]
    print(a)

    print(a[2])

    a[1] = 100
    print(a)

    a.append(4)
    # ^^^^^^ adicionar ao final
    print(a)

    a.insert(2, 10)
    print(a)

    a.remove(3)
    print(a)
    # a.remove(3)  # -> ValueError se não existir na lista

    popado = a.pop()
    print(a, popado)
    print(f"{a.pop(1)=}")
    print(a)

    c = [a]
    c.append([90, 80, 70])
    print(f"{c=}")

    b = c[0]
    b.append(100)
    print(c)
    print(a)
    print(f"{a is b = }")

    a.extend([-1, -2, -3])
    print(a)


def main2():
    a = []
    a.append([1, 2, 3])
    a.extend([4, 5, 6])
    print(a)
    print(f"{a[0]=}")
    print(f"{a[1]=}")
    print(f"{a[2]=}")
    print(f"{a[3]=}")


if __name__ == '__main__':
    main2()

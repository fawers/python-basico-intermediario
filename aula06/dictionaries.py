def main():
    curso = {'linguagem': 'Python', 'nivel': 'basico-intermediario'}
    print(curso)
    # print(curso['num_aulas'])
    curso['num_aulas'] = 'x'
    print(curso)
    print(curso['num_aulas'])
    popado = curso.pop('nivel')
    print(curso)
    print(f"{popado=}")
    curso.update({'nivel': popado}, professor='Fabricio')
    print(f"curso pós update={curso}")


if __name__ == '__main__':
    main()

# set - conjunto

def main():
    linguagens_sei = {'python', 'scala', 'rust'}
    linguagens_mercado = {'python', 'js', 'elixir'}

    print(linguagens_sei)
    print(linguagens_mercado)

    # intersecção - conjunto com itens em comum
    # print(f"{linguagens_sei.intersection(linguagens_mercado)=}")
    print(f"{linguagens_sei & linguagens_mercado = }")

    # união - junção dos itens dos conjuntos
    # print(f"{linguagens_sei.union(linguagens_mercado)=}")
    print(f"{linguagens_sei | linguagens_mercado = }")

    # diferença - remove itens de um que existem no outro
    # print(f"{linguagens_sei.difference(linguagens_mercado)=}")
    print(f"{linguagens_sei - linguagens_mercado = }")


if __name__ == '__main__':
    main()

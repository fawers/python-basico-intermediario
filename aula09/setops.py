# print(f"{=}")

def main():
    m2 = set(range(2, 21, 2))
    m3 = set(range(3, 22, 3))

    print(f"{m2=}, {m3=}")
    print(f"{m3.difference(m2)=}")
    print(f"{m3 - m2 = }")

    print(f"{m2.union(m3)=}")
    print(f"{m2 | m3 = }")

    print(f"{m2.intersection(m3)=}")
    print(f"{m2 & m3 = }")

    print(f"{m2=}")
    m2.intersection_update(m3)
    print(f"{m2=}")

    print(f"{m2.add(2)=}")
    print(f"{m2=}")


if __name__ == '__main__':
    main()

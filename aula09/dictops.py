# print(f"{=}")

def main():
    d = {'curso': 'Python', 'nivel': 'basico-intermediario'}
    print(f"{d.update(curso='Rust', duracao='x')=}")
    d['estudantes'] = ['você']

    for key, value in d.items():
        print(f"{key=}, {value=}")

    print(f"{d.keys()=}")
    print(f"{d.values()=}")

    print(f"{d.pop('curso')=}")
    print(f"{d=}")
    print(f"{d.pop('curso', None)=}")

    print(f"{d.clear()=}")
    print(f"{d=}")

    d['professor'] = 'Fabricio'
    print(f"{d.get('estudantes')=}")
    # print(f"{d['estudantes']=}")  # KeyError
    print(f"{d.get('professor')=}")
    print(f"{d['professor']=}")

    print(f"{d.popitem()=}")
    # print(f"{d.popitem()=}")

    print(f"{d.setdefault('estudantes', [])=}")
    print(f"{d=}")
    print(f"{d.setdefault('estudantes', 'você e eu')=}")
    print(f"{d=}")

    print(f"{dict.fromkeys(['nome', 'idade'], 'uma string')=}")


if __name__ == '__main__':
    main()

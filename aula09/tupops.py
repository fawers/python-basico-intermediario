def main():
    t = ('Fabricio', 30, 30)

    print(f"{t.count(15)=}")
    print(f"{t.count(30)=}")
    print(f"{t.count('30')=}")
    print(f"{t.index(30)=}")
    print(f"{'30' in t=}")
    print(f"{30 in t=}")
    print(f"{t.count('30') > 0 = }")
    print(f"{t.count(30) > 0 = }")


if __name__ == '__main__':
    main()

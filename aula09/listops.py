def main():
    l = ['Fabricio', 'Fulano', 'Ciclano']

    print(f"{l.index('Fulano')=}")
    print(f"{l.index('Fabricio')=}")
    print(f"{l.count('Fabricio')=}")
    print(f"{l.count('Ciclano')=}")
    print(f"{'Fabricio' in l = }")

    # print(f"{=}")
    print(f"{l.append('Beltrano')=}")
    print(f"{l=}")
    print(f"{l.insert(1, 'Furicano')=}")
    print(f"{l=}")
    print(f"{l.extend(['Belano', 'Celestiano', 'Dalwiano'])=}")
    print(f"{l=}")
    print(f"{l.append(['Belano', 'Celestiano', 'Dalwiano'])=}")
    print(f"{l=}")

    # for elem in l:
    #     print(f"{elem[0]=}")

    print(f"{l[-1][1]=}")

    print(f"{l.remove('Furicano')=}")
    print(f"{l=}")
    print(f"{l.pop()=}")
    print(f"{l=}")

    l2 = l.copy()
    print(f"{l2=}")
    print(f"{l is l2 = }")
    print(f"{l2.append('Alpiano')=}")
    print(f"{l=}")
    print(f"{l2=}")

    l3 = l2
    print(f"{l3.append('Corintiano')=}")
    print(f"{l=}")
    print(f"{l2 is l3 = }")
    print(f"{l2=}")
    print(f"{l3=}")

    print(f"{l2.clear()=}")
    print(f"{l2=}")
    print(f"{l3=}")

    print(f"{l=}")
    print(f"{l.reverse()=}")
    print(f"{l=}")

    print(f"{l.sort(reverse=True)=}")
    print(f"{l=}")
    print("l4=%s" % (l4 := sorted(l)))
    print(f"{l=}")

    print(f"{l[::-1]=}")
    print(f"{l[:] is l = }")


if __name__ == '__main__':
    main()

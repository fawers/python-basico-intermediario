import io
import unittest
from unittest.mock import patch

import ex_arvore


class TestTree(unittest.TestCase):
    def test_negatives_zero(self):
        for i in range(-100, 1):
            with self.subTest(i=i):
                with patch('sys.stdout', io.StringIO()) as out:
                    ex_arvore.make_tree(i)
                    out.seek(0)
                    self.assertEqual(out.read().strip(), '')

    def test_positives(self):
        bs = "5b277b3a5e25647d272e666f726d617428272a27202a2028322a692b31292920666f72206920696e2072616e6765282564295d"
        c = bytes.fromhex(bs).decode()

        for l in (1, 2, 5, 9, 15):
            tree = '\n'.join(eval(c % (l*2-1, l))).strip()
            stars = tree.count('*')

            with self.subTest(lines=l, stars=stars):
                with patch('sys.stdout', io.StringIO()) as out:
                    ex_arvore.make_tree(l)
                    out.seek(0)
                    r = out.read().strip()

                    self.assertEqual(r.count('*'), stars,
                                     f"O número de asteriscos para esta árvore deveria ser {stars}.")
                    self.assertEqual(r, tree,
                                     f"Sua árvore parece estar torta. Você está apagando algum espaço?")

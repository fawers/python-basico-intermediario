# estilo linguagem C

def main():
    nome = 'Fabricio'
    idade = 30
    print("Olá, eu sou %(texto)s e tenho %(numero)d anos de idade." % {'numero': idade, 'texto': nome})
    #                                    ^^^^^^^^^^ número inteiro, decimal
    #                  ^^^^^^^^^ string


traducoes = {
    'en': {'greet': 'Good morning, %(name)s!'},
    'pt': {'greet': 'Bom dia, %(name)s!'},
    'de': {'greet': 'Guten morgen, %(name)s!'}
}


def programa_traduzido(nome, idioma='pt'):
    print(traducoes[idioma]['greet'] % {'name': nome})


if __name__ == '__main__':
    programa_traduzido('Python', 'ru')

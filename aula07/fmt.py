# formatação padrao do python

def main():
    # translated('Fabricio')
    # table_tuple()
    # table_dict()
    # format_int()
    # format_float()
    # format_obj()
    # format_list()
    format_dict()


def greet():
    print("Olá, sou {nome} e tenho {idade} anos de idade.".format(nome='Fabricio', idade=30))


phrases = {
    'pt': {'greet': 'Olá, {name}! Bem-vindo ao curso de {course}!'},
    'en': {'greet': 'Hello, and welcome to the {course} course, {name}!'}
}


def translated(name, language='pt'):
    course = 'Python'
    print(phrases[language]['greet'].format(name=name, course=course))


def table_tuple():
    headers = ('nome', 'linguagem')
    data = [('Fulano', 'Python'),
            ('Ciclano', 'Elixir'),
            ('Beltrano', 'Rust')]

    widths = (8, 9)

    print("| {:^{}} | {:^{}} |".format(headers[0], widths[0], headers[1], widths[1]))
    print("| {} | {} |".format('-' * widths[0], '-' * widths[1]))

    for line in data:
        print("| {:^{}} | {:^{}} |".format(line[0], widths[0], line[1], widths[1]))

    # |   nome   | linguagem |
    # | -------- | --------- |
    # | Fulano   |   Python  |
    # | Ciclano  |   Elixir  |
    # | Beltrano |   Rust    |


def table_dict():
    headers = ('nome', 'linguagem')
    data = [{'nome': 'Fulano', 'linguagem': 'C'},
            {'nome': 'Ciclano', 'linguagem': 'Haskell'},
            {'nome': 'Beltrano', 'linguagem': 'JavaScript'}]

    widths = [len(headers[0]), len(headers[1])]
    for line in data:
        nome = line['nome']
        tamanho_do_nome = len(nome)
        widths[0] = max(widths[0], tamanho_do_nome)
        widths[1] = max(widths[1], len(line['linguagem']))

    print("| {name:^{w0}} | {language:^{w1}} |".format(name=headers[0], language=headers[1],
                                                       w0=widths[0], w1=widths[1]))
    print("| {} | {} |".format('-' * widths[0], '-' * widths[1]))

    for line in data:
        print("| {name:^{w0}} | {language:^{w1}} |".format(name=line['nome'], language=line['linguagem'],
                                                           w0=widths[0], w1=widths[1]))


def format_int():
    num = 0x11ab4cde
    print("{:,d}".format(num))
    print("{0:o}".format(num))
    print("{numero:x}".format(numero=num))
    print("{:b}".format(num))


def format_float():
    flt = 3978686 / 5767980
    print("{:*^30.20%}".format(flt))


def format_obj():
    i = 97  # 97 / 1
    print("{.real}". format(i))
    print("{0.denominator}". format(i))
    print("{num.numerator}". format(num=i))


def format_list():
    l = ['Ful', 'Cic', 'Bel']
    print("primeiro da lista: {[0]}".format(l))
    print("segundo da lista: {0[1]}".format(l))
    print("terceiro da lista: {lista[2]}".format(lista=l))


def format_dict():
    d = {'interpretada': 'python', 'compilada': 'rust'}
    print("linguagem interpretada: {[interpretada]}".format(d))
    print("linguagem compilada:    {0[compilada]}".format(d))


if __name__ == '__main__':
    main()

# formatação padrao do python

def main():
    table_tuple()
    separador()
    format_int()
    separador()
    format_float()
    separador()
    format_obj()
    separador()
    format_list()
    separador()
    format_dict()


def separador():
    print()
    print('=' * 30)
    print()


def greet():
    nome = 'Fabricio'
    idade = 30
    print(f"Olá, sou {nome} e tenho {idade} anos de idade.")


def table_tuple():
    headers = ('nome', 'linguagem')
    data = [('Fulano', 'Python'),
            ('Ciclano', 'Elixir'),
            ('Beltrano', 'Rust')]

    widths = (8, 9)

    print(f"| {headers[0]:^{widths[0]}} | {headers[1]:^{widths[1]}} |")
    print(f"| {'-' * widths[0]} | {'-' * widths[1]} |")

    for line in data:
        print(f"| {line[0]:^{widths[0]}} | {line[1]:^{widths[1]}} |")

    # |   nome   | linguagem |
    # | -------- | --------- |
    # | Fulano   |   Python  |
    # | Ciclano  |   Elixir  |
    # | Beltrano |   Rust    |


def table_dict():
    headers = ('nome', 'linguagem')
    data = [{'nome': 'Fulano', 'linguagem': 'C'},
            {'nome': 'Ciclano', 'linguagem': 'Haskell'},
            {'nome': 'Beltrano', 'linguagem': 'JavaScript'}]

    widths = [len(headers[0]), len(headers[1])]
    for line in data:
        nome = line['nome']
        tamanho_do_nome = len(nome)
        widths[0] = max(widths[0], tamanho_do_nome)
        widths[1] = max(widths[1], len(line['linguagem']))

    print("| {name:^{w0}} | {language:^{w1}} |".format(name=headers[0], language=headers[1],
                                                       w0=widths[0], w1=widths[1]))
    print("| {} | {} |".format('-' * widths[0], '-' * widths[1]))

    for line in data:
        print("| {name:^{w0}} | {language:^{w1}} |".format(name=line['nome'], language=line['linguagem'],
                                                           w0=widths[0], w1=widths[1]))


def format_int():
    num = 0x11ab4cde
    print(f"{num:,d}")
    print(f"{num:#o}")
    print(f"{num:#x}")
    print(f"{num:#b}")


def format_float():
    flt = 3978686 / 5767980
    print(f"{flt:*^30.20%}")


def format_obj():
    i = 97  # 97 / 1
    print(f"{i.real}")
    print(f"{i.denominator}")
    print(f"{i.numerator}")


def format_list():
    l = ['Ful', 'Cic', 'Bel']
    print(f"primeiro da lista: {l[0]}")
    print(f"segundo da lista: {l[1]}")
    print(f"terceiro da lista: {l[2]}")


def format_dict():
    d = {'interpretada': 'python', 'compilada': 'rust'}
    print(f"linguagem interpretada: {d['interpretada']}")
    print(f"""linguagem compilada:    {d["compilada"]}""")


if __name__ == '__main__':
    main()

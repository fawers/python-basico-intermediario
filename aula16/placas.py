class ANSI:
    def __init__(self, descricao: str, codigo: str, /):
        self.descricao = descricao
        self.codigo = codigo


class Placa:
    ANSI_PRETO = ANSI('FG preto BG branco', '\x1b[48;5;231;30;1m')
    ANSI_VERMELHO = ANSI('FG vermelho BG branco', '\x1b[48;5;231;38;5;196;1m')
    ANSI_RESET = ANSI('reset', '\x1b[0m')

    def __init__(self, alpha: str, /, *, cor='preto', tipo='carro'):
        self.alpha = alpha
        self.cor = cor
        self.tipo = tipo

    def cor_ansi(self) -> ANSI:
        return {
            'preto': Placa.ANSI_PRETO,
            'vermelho': Placa.ANSI_VERMELHO
        }[self.cor]

    def exibir(self):
        if self.tipo == 'moto':
            cima = f" {' '.join(self.alpha[:3])} "
            baixo = ' '.join(self.alpha[3:])
            alpha = f"{cima}{Placa.ANSI_RESET.codigo}\n{self.cor_ansi().codigo}{baixo}"

        else:
            alpha = self.alpha

        print(f"{self.cor_ansi().codigo}{alpha}{self.ANSI_RESET.codigo}")


def main():
    abc = Placa('ABC0D12')
    print(f"{abc.alpha=}, {abc.cor=}, {abc.tipo=}")
    abc.exibir()

    xyz = Placa('XYZ9F22', cor='vermelho')
    xyz.exibir()

    print()
    fws = Placa('FWS4i20', tipo='moto')
    fws.exibir()

    aws = fws
    aws.alpha = 'A' + aws.alpha[1:]

    print()
    aws.exibir()
    print()
    fws.exibir()

    print(f"{aws is fws = }")
    print(f"{aws is abc = }")
    print(f"{aws is xyz = }")
    print(f"{abc is xyz = }")


if __name__ == '__main__':
    main()

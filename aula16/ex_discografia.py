class Artista:
    def __init__(self, nome: str, ano_fundacao: int, albuns: list['Album']):
        self.nome = nome
        self.criacao = ano_fundacao
        self.albuns = albuns


class Album:
    def __init__(self, nome: str, ano_lancamento: int, musicas: list['Musica']):
        self.nome = nome
        self.ano = ano_lancamento
        self.musicas = musicas

    def duracao(self):
        pass  # calculada?


class Musica:
    def __init__(self, nome: str, duracao_segundos: int):
        self.nome = nome
        self.duracao = duracao_segundos

class Pessoa:
    def __init__(self, nome: str, idade: int):
        self.nome = nome
        self.idade = idade

    def operar(self):
        print(f"operando em Pessoa({self.nome!r}, {self.idade})")


def operar_pessoa(pessoa: tuple[str, int] | dict[str, str | int]):
    print(f"operando em {pessoa = }")


def main():
    p1 = 'Fabricio', 30
    p2 = 'Fulano', 35
    p3 = 'Ciclano', 28
    p4 = {'nome': 'Beltrano', 'idade': 25}
    p5 = Pessoa("Dalsiano", 32)
    p6 = Pessoa("Eliano", 40)

    operar_pessoa([1, 2, 3])
    operar_pessoa(p5)
    p5.operar()
    Pessoa.operar(p5)
    print(f"{p5.nome=}, {p5.idade=}")

    p6.operar()
    Pessoa.operar(p6)
    print(f"{p6.nome=}, {p6.idade=}")


if __name__ == '__main__':
    main()

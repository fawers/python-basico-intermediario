import unittest

import ex_decadas


class TestDecadas(unittest.TestCase):
    def test_decadas(self):
        modas = {1970: 'Moicano', 1980: 'New Wave', 1990: 'Grunge', 2000: 'Baggy'}

        for decada in range(1900, 2020, 10):
            for unidade in [0, 5, 9]:
                ano = decada + unidade
                moda = modas.get(decada, '')
                with self.subTest(ano=ano, moda=moda):
                    self.assertEqual(ex_decadas.moda_da_decada(ano), moda)


if __name__ == '__main__':
    unittest.main()

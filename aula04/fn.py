# definindo uma função
def hello_world():
    print("Olá, mundo!")


# greet - saudar (saudação)
def greet(name):
    print(f"Olá, {name}!")


def hello_name(name='mundo'):
    print(f"Olá, {name}!")


# hello_name("Fabricio")

# ===================

def maior_de_idade(idade):
    return idade >= 18


def main():
    age = int(input("Qual é a sua idade? "))
    print(f"{age} anos, maior de idade: {maior_de_idade(age)}")


main()

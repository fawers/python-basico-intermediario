def overage(age):
    return age >= 18


# pode dirigir
def can_drive(age):
    # se idade for igual a 18
    if age == 18:
        print("Você pode tirar carta este ano!")

    # caso contrário, se a idade for maior ou igual a 18
    elif overage(age):
        print("Você pode dirigir.")

    # senão
    else:
        print("Você não pode dirigir.")

    print("fim função")


def main():
    age = int(input("Qual é a sua idade? "))
    can_drive(age)


main()

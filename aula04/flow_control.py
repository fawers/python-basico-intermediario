def overage(age):
    return age >= 18


def main():
    age = int(input("Qual é a sua idade? "))

    # se não for maior de idade:
    if not overage(age):
        print("Você ainda não pode dirigir.")

    # senão
    else:
        print("Você pode dirigir!")

    print("fim da main")

main()

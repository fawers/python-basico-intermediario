def situacao_imc(imc):
    # Abaixo de 17          Muito abaixo do peso
    # Entre 17 e 18,49	    Abaixo do peso
    # Entre 18,50 e 24,99	Peso normal
    # Entre 25 e 29,99	    Acima do peso
    # Entre 30 e 34,99	    Obesidade I
    # Entre 35 e 39,99	    Obesidade II (severa)
    # Acima de 40	        Obesidade III (mórbida)
    return "Muito abaixo do peso"


def calcular_imc(peso_kg, altura_metros):
    altura_quad = altura_metros * altura_metros
    imc = peso_kg / altura_quad
    return imc


def main():
    peso = float(input("Qual é o seu peso? (kg) "))
    altura = float(input("Qual é a sua altura? (m) "))
    imc = calcular_imc(peso, altura)
    print(f"{imc = }")
    print(f"Situação imc: {situacao_imc(imc)}")


if __name__ == '__main__':
    main()

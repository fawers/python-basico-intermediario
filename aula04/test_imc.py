import unittest

import ex_imc


class TestIMC(unittest.TestCase):
    def test_muito_abaixo(self):
        self.assertEqual(ex_imc.situacao_imc(16.48), 'Muito abaixo do peso')

    def test_abaixo(self):
        self.assertEqual(ex_imc.situacao_imc(18.32), 'Abaixo do peso')

    def test_normal(self):
        self.assertEqual(ex_imc.situacao_imc(20.00), 'Peso normal')

    def test_acima(self):
        self.assertEqual(ex_imc.situacao_imc(27.50), 'Acima do peso')

    def test_obesidade1(self):
        self.assertEqual(ex_imc.situacao_imc(32.10), 'Obesidade I')

    def test_obesidade2(self):
        self.assertEqual(ex_imc.situacao_imc(38.00), 'Obesidade II (severa)')

    def test_obesidade3(self):
        self.assertEqual(ex_imc.situacao_imc(42.42), 'Obesidade III (mórbida)')


if __name__ == '__main__':
    unittest.main()

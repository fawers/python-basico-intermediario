# aula 4

Os exercícios se encontram nos arquivos cujos nomes começam com `ex_`.

Para verificar se as suas soluções estão corretas, execute os arquivos `test_`: estes validarão se as saídas
de suas funções estão de acordo com o combinado na aula gravada.

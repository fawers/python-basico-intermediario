def moda_da_decada(ano):
    # década de 70: Moicano
    # 80: New Wave
    # 90: Grunge
    # 00: Baggy
    # extra: ""
    return ""


def main():
    nasc = int(input("Em que ano vc nasceu? "))
    moda = moda_da_decada(nasc)

    if moda != '':
        print(f"Você nasceu na década da moda {moda}")

    else:
        print("Não sei qual foi a moda da sua década! :(")


if __name__ == '__main__':
    main()

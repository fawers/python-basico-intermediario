# operadores aritméticos
x = 5.5
y = 7
z = -4

# adição
# print("adição")
# print(x + y)
# print(z + x)

# subtração
# print("\nsubtração")
# print(z - x)
# print(y - z)

# multiplicação
# print("\nmultiplicação")
# print(x * y)

# divisão
# print("\ndivisão")
# print(y / z)

# divisão inteira
# print(x // 2)

# operadores de atribuição
# x = 12
# ^ op de atribuição
# atribui valor `12` à variável `x`

# print(x)
# x += 3  # x = x + 3
# print(x)
# x -= 5  # x = x - 5
# print(x)
# x /= 2
# print(x)
# x *= 4
# print(x)
# x //= 3
# print(x)

# módulo (resto de divisão)
# print(20 / 3)
# print(20 // 3)
# print(20 % 3)
# print(6 * 3 + 2)
#
# potência (x^y)
# print(2 ** 5)

x = 20
x %= 3  # x = x % 3
print(x)

y = 2
y **= 6  # y = y ** 6
print(y)

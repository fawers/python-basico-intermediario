# operadores de comparação
# retornam / devolvem BOOLEANOS (verdadeiro ou falso)
x = 3
y = 7

# maior que
print(f"{x > y = }")

# menor que
print(f"{x < y = }")

# maior que ou igual a
print(f"{x >= y = }")

# menor que ou igual a
print(f"{x <= y = }")

# ==================
print("====================")
a = 'FABRICIO'
b = 'Fabricio'.upper()

# igualdade
print(f"{a == b = }")

# diferença
print(f"{a != b = }")

# is (identidade)
c = 'Fabr' + 'icio'
print(f"{id(a)=}")
print(f"{a is b = }")
print(f"{id(b)=}")
print(f"{a is c = }")
print(f"{id(c)=}")
print(f"{b is c = }")

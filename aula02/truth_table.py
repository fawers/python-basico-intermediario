# TABELAS VERDADE
# E (AND)
# p | q | p and q
# V | V |    V
# V | F |    F
# F | V |    F
# F | F |    F

print("   AND")
print(f"{True and True =   }")
print(f"{True and False =  }")
print(f"{False and True =  }")
print(f"{False and False = }")

# OU (OR)
# p | q | p or q
# V | V |   V
# V | F |   V
# F | V |   V
# F | F |   F

print("\n   OR")
print(f"{True or True =   }")
print(f"{True or False =  }")
print(f"{False or True =  }")
print(f"{False or False = }")

# NEGAÇÃO (NOT)
print("\n   NOT")
print(f"{not True = }")
print(f"{not False = }")

p = True
q = False
r = True or False and p
s = q or r
print(f"{p=}, {q=}, {r=}, {s=}")

# operadores booleanos

name = 'Fabricio'
age = 16
maior_de_idade = age >= 18  # True
nome_mais_de_10_caracteres = len(name) > 10  # False
#                            ^^^  length (tamanho, comprimento)

# and (e)
print(f"{maior_de_idade and nome_mais_de_10_caracteres = }")

# or (ou)
print(f"{maior_de_idade or nome_mais_de_10_caracteres = }")

# not (não, negação)
underage = age < 18
print(f"{not underage = }")

print(f"{len(name) <= 10 and (age > 30 or underage) = }")

print(f"{True or True and False = }")

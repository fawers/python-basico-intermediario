# tipos de dados

# str (string) [texto]

texto1 = "olá, mundo!"
texto2 = 'hola, mundo!'

texto3 = "esse texto tem uma \n quebra de linha"
texto4 = 'esse texto\nquebra linhas'

texto5 = """esse texto
quebra linhas
e não possui
barra n
"""

texto6 = '''esse texto
quebra linhas
e usa aspas triplas simples'''

# print(texto5)
# valor_str = input("Qual é a sua idade? ")
# print(type(valor_str))

# ======================
# int (números inteiros)
x = 10
y = 10
z = x + y
# print(z)

# print('5' + '10')

# age = int(input('Qual é a sua idade? '))
# print(age - 18)

# ======================
# float (floating point, ponto flutuante) [racionais, quebrados]
# pi = 3.14
# print(pi / 2)
# num = 1.00000000000000000000000000000000000000000000000000000000000000000000000001
# print(num * 5)
#
# print(3.14 - 5)

# milimetros = 4e-3 # 4 * 10^(-3)
# print(milimetros)
# quilometros = 5.25e3
# print(quilometros)

# ======================
# bool (booleano) [verdadeiro ou falso]
# underage = True  # verdadeiro
# print(underage)
# sunny = False  # falso
# print(sunny)

# ======================
# None (nada)
nada = None
# print(type(nada))

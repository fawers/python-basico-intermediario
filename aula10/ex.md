1. Refaça o exercício da aula 3 utilizando funções, tratamento de erros e, opcionalmente, estruturas de dados
como listas ou dicionários.

2. Refaça os exercícios da aula 4 utilizando tratamento de erros.

3. Refaça o exercício da tabuada da aula 5 retornando uma lista com os valores em vez de imprimir na tela.

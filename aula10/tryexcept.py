def main():
    try:  # tente:
        age = int(input("Qual é a sua idade? "))
        print(f"Você tem {age} anos!")

    except ValueError:
        print("Esta não é uma idade válida...")


if __name__ == '__main__':
    main()

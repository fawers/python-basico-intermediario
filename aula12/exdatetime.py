# standard library

import datetime


def main():
    agora = datetime.datetime.now()
    um_dia = datetime.timedelta(1)
    jornada_trabalho = datetime.timedelta(hours=8)
    almoco = datetime.timedelta(hours=1)
    amanha = (agora + um_dia).replace(hour=9, minute=0, second=0, microsecond=0)

    print(agora)
    print(um_dia)
    print(agora + um_dia)
    print(agora - um_dia)
    print(f"jornada de trabalho={jornada_trabalho}")

    print(f"amanha={amanha}")
    print(f"amanha, fim do dia={amanha + jornada_trabalho + almoco}")


if __name__ == '__main__':
    main()

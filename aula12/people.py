def create_person(name: str, age: int):
    return {
        'name': name,
        'age': age
    }


def celebrate_birthday(person: dict):
    person['age'] += BIRTHDAY


BIRTHDAY = 1


# if __name__ == '__main__':
#     person = create_person("Fulano", 25)
#     print(person)
#     celebrate_birthday(person)
#     print(person)

print(f"{__file__=}, {__name__=}")

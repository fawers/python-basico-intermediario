import random
import datetime


l = ['Fulano', 'Ciclano', 'Beltrano']


print(f"{random.randint(1, 6)=}")
print(f"{random.random()=}")
print(f"{random.choice(l)=}")

r = random.Random(datetime.datetime.now().timestamp())
# r.seed(1992)


for i in range(5):
    print(f"{i}: {r.randint(1, 20)=}")

from pprint import pprint


linguagem = 'Python'
nivel = 'basico-intermediario'

curso = {
    'linguagem': 'Python',
    'nivel': 'basico-intermediario',
    'nome': f"{linguagem}: do {' ao '.join(nivel.split('-'))}",
    'professor': 'Fabricio',
    'num_aulas': 17
}

# pretty
pprint(curso)

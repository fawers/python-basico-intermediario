import people


def main():
    person = people.create_person("Fabricio", 30)
    print(person)
    print(person['name'])
    people.celebrate_birthday(person)
    print(person)
    print(people.BIRTHDAY)


if __name__ == '__main__':
    main()

print(f"{__file__=}, {__name__=}")

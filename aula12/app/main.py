import escola.matematica
import escola.ingles
import escola.portugues
import escola.ciencias.biologia
from escola import geografia, historia
from escola.ciencias import fisica, quimica


if __name__ == '__main__':
    nome_arquivo = __file__.partition('app/')[2]
    nome_modulo = __name__

    print(f"{nome_arquivo=}, {nome_modulo=}")

import struct


PNG_FILE = "../aula09/img00.png"


def main():
    print(f"{PNG_FILE=}")
    f = open(PNG_FILE, "rb")
    # 137 80 78 71 13 10 26 10
    signature = f.read(8)
    print(f"{signature=}")

    type = b''

    while type != b'IEND':
        length = struct.unpack('>I', f.read(4))[0]
        type = f.read(4)
        data = f.read(length)
        crc = f.read(4)

        print(f"{length=}, {type=}, {data=}, {crc=}")

    f.close()


if __name__ == '__main__':
    main()

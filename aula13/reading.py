# print(f"{=}")

LYRICS_FILE = "we-could-have-a-good-time.txt"


def main():
    f = open(LYRICS_FILE)
    print(f"{f.tell()=}")
    print(f"{f.readline()=}")
    print(f"{f.tell()=}")
    print(f"{f.readline()=}")
    print(f"{f.tell()=}")

    print(f"{f.seek(0)=}")
    print(f"{f.readline()=}")
    print(f"{f.tell()=}")
    f.close()


if __name__ == '__main__':
    main()

# print(f"{=}")

FILE = "writing.txt"


def main():
    f = open(FILE, 'w')
    # w -> apaga o arquivo e escreve do zero
    # a -> append, adiciona ao fim do arquivo
    print("escrevendo no arquivo com print", file=f)
    # f.write("escrevendo no arquivo com f.write")

    f.writelines([
        "so when the world feels dark\n",
        "and friends seem hard to find\n"
    ])

    linhas = [
        "you know how to light a spark",
        "just to let your worries unwind"
    ]

    for linha in linhas:
        print(linha, file=f)

    f.close()


if __name__ == '__main__':
    main()

import csv
from pprint import pprint


def read():
    with open("cadastros.csv") as f:
        reader = csv.reader(f, delimiter=';')
        regs = []

        keys = next(reader)

        for values in reader:
            reg = dict(zip(keys, values))
            regs.append(reg)

    pprint(regs)


def write():
    regs = [{'nome': 'Fulano', 'curso': 'Python'},
            {'curso': 'Elixir', 'nome': 'Ciclano'},
            {'curso': 'Haskell', 'nome': 'Beltrano'},
            {'nome': 'Dalsiano', 'curso': 'Rust'},
            {'nome': 'Eliano', 'curso': 'JavaScript'}]

    with open("cadastros.csv", "w") as f:
        writer = csv.writer(f, delimiter=";")

        headers = list(regs[0].keys())
        writer.writerow(headers)

        for reg in regs:
            line = []
            for k in headers:
                line.append(reg[k])

            writer.writerow(line)


if __name__ == '__main__':
    write()

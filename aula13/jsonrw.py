import json
from pprint import pprint


def read():
    with open("cadastros.json") as f:
        data = json.load(f)
        regs = data['cadastros']

    pprint(regs)


def readS():
    j = """{"curso": "Elixir", "duracao": "infinito"}"""
    data = json.loads(j)
    pprint(data['duracao'])


def write():
    regs = [{'nome': 'Fulano', 'curso': 'Python'},
            {'curso': 'Elixir', 'nome': 'Ciclano'},
            {'curso': 'Haskell', 'nome': 'Beltrano'},
            {'nome': 'Dalsiano', 'curso': 'Rust'},
            {'nome': 'Eliano', 'curso': 'JavaScript'}]

    with open("cadastros.json", "w") as f:
        data = {'cadastros': regs}
        json.dump(data, f, indent=4)


def writeS():
    data = {'curso': 'Python'}
    j = json.dumps(data, indent=4)
    print(j)


if __name__ == '__main__':
    readS()

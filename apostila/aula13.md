# Leitura e escrita de arquivos de texto
Nesta aula aprendemos a ler e escrever arquivos de texto e binário. Mexemos com as bibliotecas `json` e `csv` para
escrita e leitura de arquivos de texto, e `struct` para leitura de arquivos binários. Lemos um arquivo PNG inteiro!

Mas o que nos permitiu fazer tudo isso foi a função `open`, que abre arquivos de texto ou binário, para leitura,
escrita, ou adição, dependendo de qual modo usarmos para abrir os arquivos. O modo é uma string que passamos a
`open`.

| Modo de abertura | Descrição                               |
|:----------------:|-----------------------------------------|
|      `"r"`       | Leitura                                 |
|      `"rb"`      | Leitura, binário                        |
|      `"w"`       | Escrita                                 |
|      `"wb"`      | Escrita, binário                        |
|      `"x"`       | Escrita, apenas se o arquivo não existe |
|      `"a"`       | Adição                                  |
|      `"ab"`      | Adição, binário                         |
|      `"r+"`      | Leitura e adição                        |
|     `"r+b"`      | Leitura e adição, binário               |

Os modos de escrita (`w`) apagam todo o conteúdo do arquivo ao abrí-lo. Os modos de adição (`a`, _append_) adicionam
ao final do arquivo. O modos `r+` são os únicos que permitem leitura e escrita do arquivo. Finalmente, o modo `x`
só funciona se o arquivo não existir; ele força um erro caso o arquivo já exista no sistema.

A função `open` retorna um objeto com propriedades de arquivos. Por ele podemos ler ou escrever, buscar uma posição
no arquivo, reler e, mais importante, fechá-lo quando terminarmos de usá-lo.

```python
arquivo = open("nome do arquivo.txt")  # <-- por padrão o modo é 'r'
# ler o que precisa ser lido
arquivo.close()
```

Alternativamente, aprendemos também que podemos usar um bloco `with` para que o fechamento do arquivo seja feito
de forma automática ao terminar o seu bloco de código.

```python
with open("arquivo.txt", 'w') as arq:
    # escreve algo no arquivo
    arq.write("olá, ")
    print("mundo!", file=arq)

# ao fim do bloco, o arquivo é fechado automaticamente.
```

## Trabalhando com arquivos CSV
Arquivos CSV, ou _Comma Separated Values_ (valores separados por vírgula), são uma versão super simplística de uma
planilha de Excel. Eles são arquivos de texto que podem ser interpretados por programas de planilha pois todos os
seus valores são separados por um delimitador comum. Originalmente, em inglês, esse delimitador é a vírgula, e por
isso o formato tem este nome. No Brasil, o delimitador padrão é o ponto-e-vírgula (`;`).

Para trabalhar com arquivos csv, importamos o módulo `csv` e criamos um leitor (`csv.reader`) ou escritor
(`csv.writer`) dependendo da nossa necessidade. Precisamos passar o arquivo do qual ele vai ler ou para o qual ele
vai escrever, então é comum que façamos tudo relacionado ao escritor ou leitor dentro do bloco `with` do arquivo.

```python
import csv

regs = []

with open("registros.csv") as f:
    reader = csv.reader(f, delimiter=';')  # <-- 'f' só é válido dentro do bloco do 'with'
    
    for line in reader:
        # line é uma linha do arquivo.
        # no Python, é uma tupla com os valores da linha, semelhante
        # a uma chamada a tuple(linha_inteira.strip().split(';').
        regs.append(line)

with open("registros.csv", 'w') as f:
    writer = csv.writer(f, delimiter=';')

    for reg in regs:
        writer.writerow(reg)

    writer.writerows(regs)
```

## Trabalhando com arquivos JSON
O JSON é um formato de arquivo que se popularizou muito nas últimas décadas e praticamente substituiu o XML. O
formato em si é muito parecido com os dicionários do Python, então é relativamente fácil converter de e para JSON.
As funções do módulo que nos interessam são `load`, `loads`, `dump` e `dumps`. `load` lê um arquivo contendo JSON
e o converte para tipos do Python. `dump` converte tipos do Python para JSON e escreve no arquivo especificado.
`loads` e `dumps` fazem as mesmas coisas, mas em vez de ler de ou escrever para um arquivo, as operações são feitas
diretamente com strings. `loads` carrega dados de uma string, `dumps` transforma dados em uma string.

```python
import json

discografia = {
    "artistas": [
        {
            "nome": "Sem Nome",
            "fundacao": 1900,
            "albuns": [...]
        }
    ]
}

# exportando como string
disc_json = json.dumps(discografia)
# escrevendo em um arquivo
with open("discografia.json", 'w') as f:
    json.dump(discografia, f)

# importando de uma string
discog = json.loads(disc_json)
# lendo de um arquivo
with open("discografia.json") as f:
    discog = json.load(f)
```

***

[← aula 12](./aula12.md) | [aula 14 →](./aula14.md)

# Testes unitários
Nesta aula, finalmente aprendemos a ler e entender a correção dos exercícios: aprendemos a testar os nossos
programas.

Existem vários tipos de teste de software. Hoje conversamos sobre os testes unitários. Testes unitários testam
unidades, e unidades são o menor pedaço de código em software. Normalmente, uma unidade se refere a uma função.
Então, testes unitários testam funções individuais.

Quando escrevemos um teste, o escrevemos com um resultado final em mente. "Quando eu chamar esta função com _estas_
entradas, ela deve me devolver _este_ resultado." Por exemplo, quando eu chamar a função `soma(a, b)` com os
argumentos 2 e 3, ela deve me retornar 5. Ou quando eu pedir um registro em um arquivo csv (que eu sei que
existe), a função deve me retornar este registro. Ou quando eu pedir um registro que não existe, ela deve me retornar
`None`.

Normalmente testes unitários não usam dados reais, e sim dados fictícios manipulados para aquele caso de teste. Se
existe uma função que opera em um banco de dados, então o teste que testa essa função deve montar a estrutura do
banco de dados igual à real, mas com dados fictícios. Suponhamos que haja uma função que diz se um arquivo é um
arquivo de imagem PNG válido - vimos como ler um arquivo PNG na aula 13. A função verifica
se o arquivo contém o cabeçalho PNG e retorna `True` caso sim, `False` caso contrário.

```python
# png.py
def png_valido(nome_arquivo: str) -> bool:
    cabecalho_png = bytes([137, 80, 78, 71, 13, 10, 26, 10])

    try:
        with open(nome_arquivo, 'rb') as png:
            return png.read(len(cabecalho_png)) == cabecalho_png

    except FileNotFoundError:
        return False
```

Temos 3 possíveis caminhos na nossa função:

1. O arquivo é um arquivo PNG válido e os seus 8 primeiros bytes são iguais ao cabeçalho que definimos;
2. O arquivo não é um arquivo PNG válido, seus 8 primeiros bytes não são iguais ao cabeçalho;
3. O arquivo não existe.

Apenas no caso 1 a função devolve `True`.

3 caminhos existirem significa que temos 3 casos de teste. Um bom teste cobrirá todos os possíveis casos.
Usaremos dois arquivos, um PNG e um JPEG (ou qualquer outro tipo de arquivo que não um PNG) para testar os 3 casos.

```python
# test_png.py
import unittest

from png import png_valido


class TestPNGValido(unittest.TestCase):
    def test_png_valido(self):
        arquivo = 'a.png'
        self.assertTrue(png_valido(arquivo))

    def test_png_invalido(self):
        arquivo = 'b.jpg'
        self.assertFalse(png_valido(arquivo))

    def test_arquivo_inexistente(self):
        arquivo = 'c.nao.existe'
        self.assertFalse(png_valido(arquivo))
```

\* Substitua `a.png` e `b.jpg` por arquivos reais no sistema. Certifique-se de que o primeiro é um PNG e o segundo
não.

- O primeiro caso, `test_png_valido`, testa o "caminho feliz" da nossa função: o arquivo existe, e os seus 8 primeiros
  bytes são o cabeçalho PNG.
- O segundo caso, `test_png_invalido`, testa o segundo caminho feliz: o arquivo existe, mas seus 8 primeiros bytes não
  são o cabeçalho PNG.
- O terceiro caso testa o caminho triste: o arquivo não existe e a função captura uma exceção, retornando `False`.

## Observações
Perceba que a nomenclatura importa: o nome do arquivo de código-fonte de testes unitários precisa começar com
`test`, assim como o nosso `test_png.py`. O nome da classe também precisa começar com `Test`, e os métodos de teste
precisam começar com `test_`. Desta forma o Python sabe o que é teste e o que não é.

***

Temos variados exemplos de testes unitários nas pastas das aulas; estude-os!

***

[← aula 16](./aula16.md)

# Operações de strings
Nesta aula vimos diversas operações de strings que podemos fazer por meio de seus métodos. Em vez de
montar uma tabela, agruparemos as operações e falaremos sobre os grupos.

- Acessos e fatiamento
  - Podemos acessar e fatiar strings por meio dos colchetes, e dos números dentro deles. Assim como com
    listas, podemos acessar ou fatiar por índices.
  - `str[i]`

    ```python
    >>> 'Python'[0]
    'P'
    >>> 'Python'[-1]
    'n'
    ```
    
  - `str[i:f:p]`

    ```python
    >>> 'Python[1:]'
    'Python[1:]'
    >>> 'Python'[1:]
    'ython'
    >>> 'Python'[:-1]
    'Pytho'
    >>> 'Python'[::2]
    'Pto'
    >>> 'Python'[::-1]
    'nohtyP'
    >>> 'Python'[1:-1:2]
    'yh'
    ```

- `is_`
  - A família de métodos `is_` retorna os valores booleanos `True` e `False`. Elas realizam testes na string.
    - `isupper`, `islower`, `istitle`
      - Estas operações verificam se os caracteres alfabéticos da string são todos maiúsculos, minúsculos ou
        em formato de título (primeira letra de cada palavra maiúscula, resto minúsculo).
    - `isalpha`, `isalnum`, `isdigit`
      - Estas operações verificam se a string é alfabética (só letras), composta somente por dígitos, ou
        alfa-numéricas (letras e dígitos).

- `strip`
  - O grupo _strip_ cuida de remover caracteres do começo, do fim, ou de ambos os lados da string. Normalmente
    o método limpa espaços em branco, mas é possível especificar quais caracteres queremos remover.

    ```python
    >>> "  Python  ".strip()
    'Python'
    >>> "  Python  ".lstrip()  # l = left ←
    'Python  '
    >>> "  Python  ".rstrip()  # r = right →
    '  Python'
    ```

- `split` e `join`
  - split e join são as operações desejadas quando se quer quebrar uma string em uma lista delas, ou juntar
    uma lista de strings em uma só. Split, ao receber um delimitador (um "caractere de quebra"), quebrará
    a string toda vez que este caractere for encontrado. Join faz o inverso: nós informamos o delimitador
    ("caractere de fixação") e damos uma lista de strings para que ele "cole" as strings com o delimitador.
    Split presume o espaço em branco como delimitador padrão.

    ```python
    >>> "strings-com-traços".split('-')
    ['strings', 'com', 'traços']
    >>> '-'.join(["strings", "separadas"])
    'strings-separadas'
    ```

  - Ainda é possível passar um número a split, dizendo quantas vezes queremos que a string seja quebrada.
    
    ```python
    >>> "config=chave=valor".split('=', 1)
    ['config', 'chave=valor']
    ```

  - Também existe o `rsplit`, que faz a quebra da string começando pelo fim dela. Útil quando temos um número
    fixo de quebras para fazer.
    
    ```python
    >>> "pacotealto.pacotebaixo.modulo.funcao".rsplit('.', 1)
    ['pacotealto.pacotebaixo.modulo', 'funcao']
    ```

- `partition`
  - partition funciona de uma forma semelhante ao split, mas sempre quebra a string uma vez só. Tanto
    `partition` quanto `rpartition` retorna uma tupla de três elementos (uma "tripla"), em que o elemento do
    meio é o delimitador, e os outros dois elementos são os dois "lados" da string.

    ```python
    >>> "config=chave=valor".partition('=')
    ('config', '=', 'chave=valor')
    >>> "2 ** 2 ** 2".rpartition('**')
    ('2 ** 2 ', '**', ' 2')
    ```



- `upper`, `lower`, `title`, `capitalize`
  - Todas as operações acima manipulam a caixa dos caracteres alfabéticos. `upper` deixa todas as letras em
    caixa alta (maiúsculas), `lower` em caixa baixa (minúsculas), `title` deixa a primeira letra de cada
    palavra em caixa alta e o resto em caixa baixa, e `capitalize` deixa apenas a primeira letra maiúscula.

    ```python
    >>> "pyThon baSics".upper()
    'PYTHON BASICS'
    >>> "pyThon baSics".lower()
    'python basics'
    >>> "pyThon baSics".title()
    'Python Basics'
    >>> "pyThon baSics".capitalize()
    'Python basics'
    ```

- `startswith`, `endswith`
  - Esses métodos verificam a existência de um prefixo ou sufixo, respectivamente.

    ```python
    >>> "Intuição".startswith("int")
    False
    >>> "Intuição".lower().startswith("int")
    True
    ```

- `ljust`, `rjust`, `center`
  - Estes são métodos de formatação. `ljust` e `rjust` justificam a string à esquerda ou à direita,
    respectivamente. `center` centraliza.
  
    ```python
    >>> 'python'.ljust(10)
    'python    '
    >>> 'python'.rjust(10)
    '    python'
    >>> 'python'.center(10)
    '  python  '
    ```

- `replace`
  - `replace` substitui as ocorrências de uma substring com outra substring.

    ```python
    >>> 'programação'.replace('programa', 'intui')
    'intuição'
    ```

***

[← aula 7](./aula07.md) | [aula 9 →](./aula09.md)

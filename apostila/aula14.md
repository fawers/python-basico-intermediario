# Recursão e Pattern Matching
Nesta aula vimos sobre recursão e pattern matching.

A recursão é uma forma de computação que faz uso dos recursos da própria linguagem para criar um "loop" usando
funções. A recursão é o principal meio de computação repetitiva que linguagens funcionais como Haskell usam. Se
trata de fazer uma função chamar a si mesma para continuar uma computação, parando quando o programa atingir uma
condição sentinela de parada.

Considere a seguinte função que faz uma contagem regressiva:

```python
def countdown(n: int):
    if n < 1:
        return

    print(n)
    countdown(n-1)
```

Perceba a chamada na última linha da função: `countdown(n-1)`. Significa que a função chamará a si mesma até que
a sentinela, `n < 1` na primeira linha, seja verdadeira. Podemos implementar um somatório de lista usando recursão
da seguinte maneira:

```python
def somatorio(n: int) -> int:
    if n <= 0:
        return 0

    return n + somatorio(n-1)
```

Ou a soma dos elementos de uma lista com pattern matching:

```python
def soma(nums: list[int]) -> int:
    match nums:
        case []:
            return 0

        case [n, *ns]:
            return n + soma(ns)
```

Já sobre Pattern Matching, você pode ler um artigo que escrevi sobre [aqui](https://github.com/Fawers/pattern-matching-in-python).

***

[← aula 13](./aula13.md) | [aula 16 →](./aula16.md)

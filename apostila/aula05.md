# Controle de fluxo com loops
Nesta aula aprendemos a controlar o fluxo dos nossos programas por meio dos loops _while_ e _for_, usados
para repetir blocos de código baseado em uma expressão booleana ou alguma sequência de valores,
respectivamente.

## Repetindo blocos de código enquanto uma condição é verdadeira
Usamos a estutura `while` para repetir um bloco de código de acordo com a veracidade de uma expressão booleana.
O bloco se repete _enquanto_ a condição for verdadeira.

Para visualizar como essa estrutura funciona, imagine que você se encontra, por exemplo, no 3º andar de um
prédio sem elevadores e quer descer até o térreo. Uma das formas de se chegar lá é:

```
entrar na escadaria

enquanto o andar atual não for o zero:
    descer um andar
    
sair da escadaria
```

![usuário descendo lance de escadas de prédio de 3 andares](./img/aula5/img00.png "usuário descendo as escadas")

O loop termina quando a condição se torna falsa. No nosso exemplo, quando o usuário chega no andar 0.

## Repetindo blocos de código para cada ítem de uma sequência
Quando temos uma sequência, como um intervalo de números com `range`, caracteres de uma string, ou elementos
de uma lista, tupla, ou dicionário, podemos percorrer cada ítem da sequência com a estrutura `for`. Se você
for a um evento de amostras de bolo de uma confeitaria, você pode experimentar uma fatia de cada bolo com a
ajuda de um `for`:

```
para cada amostra na mesa de bolos:
    pegar a amostra
    comer
```

![usuário olhando para mesa de bolos e babando](./img/aula5/img01.png "usuário pronto pra engordar")

O loop termina quando chegamos ao fim da sequência. No nosso exemplo, depois de comer a última amostra da
mesa.

# Reiniciando ou interrompendo o loop
Existem duas instruções que podemos usar em qualquer um dos dois loops e que servem para manipular o fluxo.
Um deles permite que reiniciemos o bloco de código enquanto o outro interrompe completamente o loop.

Usamos a instrução `continue` para _continuar_ para a próxima iteração ("volta") do loop. É necessário
cuidado ao usá-la dentro de um loop `while` pois podemos facilmente criar loops infinitos se não nos
atentarmos à condição de parada.

Usamos a intrução `break` para interromper completamente ("_quebrar_ o fluxo do") loop. Com `break`, nem a
condição do `while` e nem o próximo elemento da sequência do `for` são avaliados; o loop termina e o programa
segue para a próxima instrução.

***

[← aula 4](./aula04.md) | [aula 6 →](./aula06.md)

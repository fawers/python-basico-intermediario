# Operações de coleções
Nesta aula vimos as várias operações de tuplas, listas, dicionários e conjuntos.

## Operações de tuplas e listas
Suponhamos a lista `l = ['a', 'b', 'c']` e tupla `t = ('a', 'b', 'c')`. Podemos realizar as seguintes
operações com elas:

<sup>\* apenas as operações de acesso, fatiamento, contagem e busca por índice existem para tuplas.)</sup>

| Operação   | Descrição                                                                               |        Exemplo         |            Resultado             |
|------------|-----------------------------------------------------------------------------------------|:----------------------:|:--------------------------------:|
| Acesso     | Acessa o elemento no índice.                                                            |         `t[0]`         |              `'a'`               |
| Fatiamento | Fatia a sequência.                                                                      |        `t[1:]`         |           `('b', 'c')`           |
| `.count`   | Conta quantas vezes um valor aparece na sequênca.                                       |     `t.count('a')`     |               `1`                |
| `.index`   | Devolve o índice onde um valor se encontra. Produz um `ValueError` se não o encontrar.  |     `t.index('c')`     |               `2`                |
| `.append`  | Insere um elemento ao final da lista.                                                   |    `l.append('d')`     |      `['a', 'b', 'c', 'd']`      |
| `.insert`  | Insere um elemento no índice especificado.                                              |   `l.insert(0, 'z')`   |      `['z', 'a', 'b', 'c']`      |
| `.extend`  | Insere elementos de outra sequência ou percorrível ao final da lista.                   |   `l.extend('def')`    | `['a', 'b', 'c', 'd', 'e', 'f']` |
| `.remove`  | Remove um elemento da lista.                                                            |    `l.remove('b')`     |           `['a', 'c']`           |
| `.pop`     | Remove da lista o elemento no índice especificado, ou o último se não for especificado. |       `l.pop()`        |           `['a', 'b']`           |
| `.copy`    | Cria uma cópia da lista.                                                                |       `l.copy()`       |        `['a', 'b', 'c']`         |
| `.clear`   | Limpa a lista.                                                                          |      `l.clear()`       |               `[]`               |
| `.reverse` | Inverte os elementos da lista.                                                          |     `l.reverse()`      |        `['c', 'b', 'a']`         |
| `.sort`    | Ordena a lista. Opcionalmente a ordena em ordem decrescente com o parâmetro `reverse`.  | `l.sort(reverse=True)` |        `['c', 'b', 'a']`         |

## Operações de dicionários
Suponha o dicionário `d = {'um': 1, 'dois': 2}` para as operações a seguir.

| Operação   | Descrição                                                                |       Exemplo        |              Resultado              |
|------------|--------------------------------------------------------------------------|:--------------------:|:-----------------------------------:|
| Acesso     | Acessa um valor pela sua chave.                                          |      `d['um']`       |                 `1`                 |
| Inserção   | Insere um par de chave e valor no dicionário.                            |   `d['três'] = 3`    |  `{'um': 1, 'dois': 2, 'três': 3}`  |
| `.get`     | Funciona como o acesso, mas não produz um erro caso a chave não exista.  |   `d.get('zero')`    |               `None`                |
| `.update`  | Atualiza o dicionário com as chaves e valores fornecidos.                | `d.update(quatro=4)` | `{'um': 1, 'dois': 2, 'quatro': 4}` |
| `.keys`    | Retorna uma sequência com as chaves do dicionário.                       |      `d.keys()`      |          `('um', 'dois')`           |
| `.values`  | Retorna uma sequência com os valores do dicionário.                      |     `d.values()`     |              `(1, 2)`               |
| `.items`   | Retorna uma sequência com as chaves e valores do dicionário.             |     `d.items()`      |     `(('um', 1), ('dois', 2))`      |
| `.pop`     | Remove a chave especificada do dicionário e retorna o seu valor.         |    `d.pop('um')`     |                 `1`                 |
| `.popitem` | Remove um par de chave e valor do dicionário e o retorna.                |    `d.popitem()`     |            `('dois', 2)`            |

## Operações de conjuntos
Considere os conjuntos `a = {1, 2, 3, 4, 5}` e `b = {2, 4, 6}` para as seguintes operações.

| Operação      | Descrição                                                     |         Exemplo         |      Resultado       |
|---------------|---------------------------------------------------------------|:-----------------------:|:--------------------:|
| `.add`        | Adiciona um elemento ao conjunto.                             |       `a.add(6)`        | `{1, 2, 3, 4, 5, 6}` |
| Diferença     | Remove de `b` os elementos de `a`.                            |         `b - a`         |        `{6}`         |
| `.difference` |                                                               |    `b.difference(a)`    |        `{6}`         |
| Intersecção   | Cria um novo conjunto com os elementos em comum de `a` e `b`. |         `a & b`         |       `{2, 4}`       |
| `.intersect`  |                                                               |    `a.intersect(b)`     |       `{2, 4}`       |
| União         | Cria um novo conjunto com os elementos dos dois conjuntos.    | <code>a &vert; b</code> | `{1, 2, 3, 4, 5, 6}` |
| `.union`      |                                                               |      `a.union(b)`       | `{1, 2, 3, 4, 5, 6}` |

***

[← aula 8](./aula08.md) | [aula 10 →](./aula10.md)

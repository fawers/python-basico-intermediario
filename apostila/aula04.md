# Aula 4: Introdução a funções e tomada de decisões
Nesta aula vimos o básico do básico de funções e começamos a escrever programas um
pouco mais inteligentes, que conseguem tomar decisões.

## Funções: reusando blocos de código
Aqui aprendemos que podemos reutilizar blocos de código ao dar nomes a eles, que chamamos de funções.
Podemos chamar (usar) funções quantas vezes acharmos necessário, como, por exemplo, várias chamadas à
função `print`. Chamamos `print` diversas vezes pois precisamos exibir uma variedade de dados e mensagens
ao usuário. Analogamente, podemos chamar nossas próprias funções o quanto for necessário sem precisar
reescrever todo o bloco de código.

Usamos a palavra `def` para **def**inir uma função, seguido do seu nome e dos dados de entrada necessários
para que a função funcione corretamente. Depois disso, vem o bloco de código associado a esse nome.

```python
# exemplo sem dados de entrada
def ola_mundo():
    print("Olá, mundo!")

# exemplo com dados de entrada
def ola_usuario(nome):
    print(f"Olá, {nome}!")
```

No primeiro exemplo, podemos chamar a função apenas usando o seu nome, abrindo e fechando parênteses:

```python
ola_mundo()
```

Esta chamada executará o bloco de código associado, que é `print("Olá, mundo!")` que, por sua vez, exibirá
a mensagem _Olá, mundo!_ na tela.

Já no segundo exemplo, precisamos passar um dado, um valor no momento em que chamamos a função. Por exemplo,

```python
ola_usuario("Fabricio")
```

Desta forma, a string `Fabricio` será atribuída à variável `nome` e, ao substituir os valores dentro da
função, a mensagem _Olá, Fabricio!_ será exibida na tela. Tente chamar esta função passando outros valores
a ela!

Uma terceira forma de definir a mesma função é atribuindo um valor padrão às suas variáveis de entrada.

```python
def saudar(nome='mundo'):
    print(f"Olá, {nome}!")
```

Assim podemos chamar a função passando ou não valores de entrada. Se não passarmos, a função usará o seu
valor padrão.

```python
saudar()
# imprime 'Olá, mundo!' na tela.

saudar("José")
# imprime 'Olá, José!' na tela.
```

## Controlando o fluxo do programa
No mundo do desenvolvimento de software, é uma necessidade que consigamos fazer com que o programa execute
um ou outro bloco de código baseado em alguma(s) condição(ões). Para isso, usamos uma estrutura chamada
`if` ("se", em inglês) para validar condições (expressões booleanas) e executar um pedaço de código se elas
forem verdadeiras, ou opcionalmente outro bloco de código caso elas sejam falsas.

Vamos aos exemplos.

```python
if clima == 'ensolarado':
    print("vamos para a praia!")
```

Neste caso, a mensagem _vamos para a praia!_ só será exibida se o valor atribuído à variável `clima` for a
string `ensolarado`. Façamos o teste:

```python
clima = 'ensolarado'

if clima == 'ensolarado':
    print("vamos para a praia!")

clima = 'chuvoso'

if clima == 'ensolarado':
    print("vamos para o campo!")
```

Ao executar este programa, veremos que a mensagen _vamos para a praia!_ será exibida, mas a mensagem
_vamos para o campo!_ não. Brinque um pouco com este pedaço de código: use outros operadores, crie outras
variáveis e componha expressões booleanas. Teste a tabela verdade, se quiser.

### Caso contrário...
Outra instrução dessa estrutura é o `else`, "caso contrário" ou "senão" em inglês. Serve para executarmos
um bloco alternativo de código caso a expressão do `if` seja falsa. Vamos usar alguns exemplos da tabela
verdade da aula anterior.

```python
idade = int(input("Qual é a sua idade? "))
titulo = input("Você possui título de eleitor? ")
# ^^^^ responda "sim" (sem as aspas) para validar.

pode_votar = idade >= 16 and titulo == 'sim'

# o gran finale dessa seção:
if pode_votar:
    print("vc já pode enfrentar filas quilométricas para votar!")

else:  # caso contrário
    print("vc ainda não precisa se preocupar com isso.")
```

Execute o programa acima e varie os valores de `idade` e `titulo` para ver diferentes resultados.

### Mas e se...
Existe ainda uma terceira instrução, opcional, e que pode ser repetida quantas vezes for preciso: o `elif`.

```python
idade = int(input("Qual é a sua idade? "))

# se vc tiver 21 anos ou mais
if idade >= 21:
    # então
    print("vc é considerado maior de idade no mundo inteiro!")

# mas se vc tiver 18 ou mais
elif idade >= 18:
    print("vc já pode dirigir!")

# ou se vc tiver 16 ou mais
elif idade >= 16:
    print("vc pode votar.")

# caso contrário
else:
    print("vc não pode fazer muita coisa ainda.")
```

É importante que você use `elif`s em vez de `if`s pois os `elif`s sugerem que esta árvore de decisão é uma
estrutura só. Se você usar `if`s onde `elif`s deveriam ser usados, você pode introduzir bugs lógicos em
seus programas.

```python
idade = 30

# se vc tiver 21 anos ou mais
if idade >= 21:
    print("vc é considerado maior de idade no mundo inteiro!")

# se vc tiver 18 ou mais
if idade >= 18:
    print("vc já pode dirigir!")

# se vc tiver 16 ou mais
if idade >= 16:
    print("vc pode votar.")

# caso a sua idade seja menor que 16
else:
    print("vc não pode fazer muita coisa ainda.")
```

Este exemplo parece inofensivo - afinal, se você é maior de idade, então pode dirigir e votar também. Mas
no decorrer do curso veremos alguns casos em que é problemático escrever dessa forma. No primeiro exemplo,
temos apenas uma estrutura _if/elif/else_; neste, temos duas estruturas _if_ e uma _if/else_ (três no
total).

***

[← aula 3](./aula03.md) | [aula 5 →](./aula05.md)

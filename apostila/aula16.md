# Introdução à Orientação a Objetos
Nesta aula começamos a aprender como criar e usar classes para contextualizar os nossos dados e fazer uso do que
chamamos de _orientação a objetos_ no Python.

Aprendemos que devemos criar nossos próprios tipos quando queremos criar uma estrutura de dados em comum para
variadas ocasiões, mas cujos valores podem ter dados diferentes. _**Mesma estrutura, dados diferentes**_.

Criamos um tipo com a palavra `class` seguido do nome do tipo. Instanciamos, ou criamos um objeto desta classe, ao
"chamar" a classe como se fosse uma simples função. Criamos um método, ou função de classe, chamado `__init__` para
inicializar os dados do nosso objeto.

```python
class Curso:
    def __init__(self, nome, lancamento):
        self.nome = nome
        self.lancamento = lancamento


python = Curso('Python', 'fevereiro/2023')
rust = Curso('Rust', 'a ser definido')
```

No exemplo acima, ambas as variáveis `python` e `rust` são instâncias diferentes de `Curso`, ou simplesmente `Curso`s
diferentes - da mesma forma que `"oleg"` e `"dima"` são duas instâncias diferentes de string (ou duas strings
diferentes), ou da mesma forma que `{'curso': 'Python'}` e `{'nome': 'Fabricio'}` são dois dicionários diferentes.

_**Mesma estrutura, dados diferentes**_. Todos os métodos que conseguimos chamar para a string `"oleg"` também
conseguimos chamar para a string `"dima"`: `"oleg".title()`, `"dima".title()`.

No Python, usamos classes para modelar a grande, grande maioria dos nossos dados. Quer representar uma pessoa? Crie
uma classe. Representar um carro? Crie uma classe. Que tal um motor, rodas, pneus, cada um com seus próprios
atributos como durabilidade, rotações por minuto, transmissão? Classes. Exemplos:

```python
class Roda:
    def __init__(self, tamanho: float, espessura: float):
        (self.tamanho, self.espessura) = (tamanho, espessura)

    def adiante(self):
        print(f"Roda {id(self)} girando para frente.")

class Eixo:
    def __init__(self, roda_esquerda: Roda, roda_direita: Roda):
        (self.r_esq, self.r_dir) = (roda_esquerda, roda_direita)

    def adiante(self):
        self.r_esq.adiante()
        self.r_dir.adiante()
        print(f"Eixo {id(self)} girando adiante.")

class Carcaca:  # carcaça
    def __init__(self, eixo_dianteiro: Eixo, eixo_traseiro: Eixo):
        (self.ex_frente, self.ex_tras) = (eixo_dianteiro, eixo_traseiro)

    def adiante(self):
        self.ex_frente.adiante()
        self.ex_tras.adiante()
        print(f"Carcaça {id(self)} seguindo adiante.")

carcaca_carro = Carcaca(
    Eixo(Roda(10, 10), Roda(10, 10)),
    Eixo(Roda(9, 11), Roda(9, 11)))

carcaca_carro.adiante()
```

Este já é um exemplo avançado comparado com o que vimos na aula gravada; falaremos mais desta forma de montar classes
e tipos em um curso futuro.

## Atributos e métodos
Atributos são todas as variáveis que pertencem à instância de uma classe. São os atributos que descrevem o
tipo e <span title="(superficialmente)">diferenciam</span> um objeto de outro. Os atributos são os dados
contextualizados sob um novo tipo.

```python
class Pessoa:
    def __init__(self, nome: str):
        self.nome = nome

fab = Pessoa('Fabricio')
art = Pessoa('Arthur')
```

No exemplo acima, o atributo `nome` descreve o nosso tipo `Pessoa`. `fab` e `art` são dois objetos `Pessoa`
com seus próprios nomes. _**Mesma estrutura, dados diferentes**_.

Já os métodos são funções que pertencem à instância. São funções contextualizadas definidas dentro do escopo de
`Pessoa`. Dizemos que métodos são contextualizados por contarem com o parâmetro `self`, que é o próprio objeto. Ou
seja, os métodos têm acesso a todos os atributos da instância. Podemos chamar os métodos de duas formas diferentes:

```python
eixo_dianteiro.adiante()
# ou
Eixo.adiante(eixo_dianteiro)
```

No primeiro caso, o `self` se encontra à esquerda do ponto. No segundo, passamos explicitamente. Podemos ver o que
o Python vê pelo terminal interativo:

```
>>> roda = Roda(10, 10)
>>> roda
<__main__.Roda object at 0x7f34305e5360>
>>> roda.adiante
<bound method Roda.adiante of <__main__.Roda object at 0x7f34305e5360>>
>>> Roda.adiante
<function Roda.adiante at 0x7f34305dadd0>
```

`Roda.adiante` é meramente uma função, mas `roda.adiante` é um método "amarrado" a uma instância; este sabe o que
`self` é, mas o primeiro não.

***

[← aula 14](./aula14.md) | [aula 17 →](./aula17.md)

# Aula 2: Tipos de dados e operadores
Nesta aula aprendemos sobre os tipos de dados primitivos do Python e alguns de seus operadores.

## Os tipos de dados do Python
| Tipo  | Descrição                                                                    |
|-------|------------------------------------------------------------------------------|
| str   | Tipo textual. Representa qualquer valor que seja mero texto.                 |
| int   | Tipo numérico. Representa números inteiros.                                  |
| float | Tipo numérico. Representa números de ponto flutuante, ou com casas decimais. |
| bool  | Tipo booleano. Representa os valores verdadeiro (True) e falso (False).      |
| None  | Tipo _nada_. Representa a ausência de valores.                               |

### Inicialização e conversão de cada tipo
#### str
```python
s = ''
s = ""
s = """"""
s = ''''''
s = str()
s = str(x)
s = "string"
```

#### int
```python
i = 0
i = int('0')
i = int()
i = 12_345_789  # underlines como separadores de milhares
i = int('deadbeef', 16)  # conversão de string hexadecimal para int
i = int(3.14)  # conversão de float para int
```

#### float
```python
f = 0.0
f = 3.14
f = float()
f = float("3.14")
f = float(7)
f = 5e3  # 5 * 10 ^ 3
f = 314e-2  # 314 * 10 ^ -2
```

#### bool
```python
b = True
b = False
b = bool(x)
```

#### None
```python
n = None
```

## Operadores aritméticos
São os operadores que usamos para fazer contas aritméticas. Os conhecemos das aulas de matemática dos tempos
de escola. Eles são:

| Operador | Função                    | Exemplo | Resultado |
|:--------:|---------------------------|---------|-----------|
|   `+`    | Adição                    | 2 + 2   | 4         |
|   `-`    | Subtração                 | 10 - 5  | 5         |
|   `*`    | Multiplicação             | 3 * 4   | 12        |
|   `/`    | Divisão                   | 9 / 2   | 4.5       |
|   `//`   | Divisão inteira           | 9 // 2  | 4         |
|   `%`    | Módulo (resto de divisão) | 20 % 3  | 2         |
|   `**`   | Potência                  | 2 ** 4  | 16        |


## Operadores de atribuição
São os operadores que usamos para atribuir valores a variáveis. Alguns são combinações com os operadores
aritméticos e servem para economizar alguns caracteres.

| Atribuição | Valor de `x` |
|:----------:|--------------|
|  `x = 2`   | 2            |
|  `x += 3`  | 5            |
|  `x -= 2`  | 3            |
| `x *= 10`  | 30           |
|  `x /= 2`  | 15           |
| `x //= 2`  | 7            |
|  `x %= 3`  | 2            |
| `x **= 3`  | 8            |

***

[← aula 1](./aula01.md) | [aula 3 →](./aula03.md)

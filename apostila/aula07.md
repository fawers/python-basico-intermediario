# Formatando strings em Python

Nesta ~~longa e demorada~~ aula, aprendemos algumas técnicas de formatação de strings em Python. Aprendemos que,
a princípio, as três formas mais conhecidas de formatar strings são:

* C-style, um estilo de formatação que remete à linguagem de programação C;
* Método (operação) `format`;
* F-strings, strings `f`ormatadas.

## C-style
A formatação estilo C foi a primeira que vimos, e também a mais "primitiva" das três. Ela nasceu junto com o
Python, e é inspirada no método de formatação da família de funções `printf` da linguagem C:

```c
#include <stdio.h>

int main(void) {
    char *m = "mundo";
    printf("Olá, %s!\n", m);
    return 0;
}
```

### Diretivas de formatação c-style

| Diretiva de formatação | Descrição                                                                                                               |              Exemplo               |   Resultado    |
|------------------------|-------------------------------------------------------------------------------------------------------------------------|:----------------------------------:|:--------------:|
| %s                     | Formata qualquer objeto ou string.                                                                                      |         `"%s" %  "Python"`         |   `"Python"`   |
| %10s                   | Garante que a string será formatada com pelo menos 10 caracteres. Preenche com espaços à esquerda.                      |        `"%10s" % "Python"`         | `'    Python'` |
| %-10s                  | Garante que a string será formatada com pelo menos 10 caracteres. Preenche com espaços à direita.                       |        `"%-10s" % "Python"`        | `'Python    '` |
| %d                     | Formata números inteiros.                                                                                               |            `"%d" % 100`            |    `"100"`     |
| %-10d                  | Garante que a string formatada terá pelo menos 10 caracteres. Preenche com espaços à direita.                           |          `"%-10d" % 100`           | `"100       "` |
| %10d                   | Garante que a string formatada terá pelo menos 10 caracteres. Preenche com espaços à esquerda.                          |           `"%10d" % 100`           | `"       100"` |
| %010d                  | Garante que a string formatada terá pelo menos 10 caracteres. Preenche com zeros à esquerda.                            |          `"%010d" % 100`           | `"0000000100"` |
| %+d                    | Formata o número incluindo um sinal positivo se o número for maior que ou igual a zero.                                 |           `"%+d" % 100`            |    `"+100"`    |
| %f                     | Formata números de ponto flutuante.                                                                                     |           `"%f" % 3.14`            |  `"3.140000"`  |
| %6f                    | Garante que a string formatada terá pelo menos 6 caracteres.                                                            |           `"%6f" % 3.14`           |  `"3.140000"`  |
| %6.2f                  | Garante que a string formatada terá pelo menos 6 caracteres. O número exibido terá uma precisão de duas casas decimais. |          `"%6.2f" % 3.14`          |   `"  3.14"`   |
| %(nome)s               | Formata a string de pelas chaves e valores do dicionário.                                                               | `"%(nome)s" % {'nome': 'Python'}`  |   `"Python"`   |

## Método `format` e f-strings
Esses dois estilos de formatação são muito parecidos, e saber usar um normalmente significa saber usar o
outro, também. As diferenças-chave entre eles são:

* O método format não necessita que um nome seja usado na diretiva de formatação; f-strings _só_ funcionam
  com nomes porque extraem o valor da variável de mesmo nome.
* Strings modelo podem ser usadas com o método format porque conseguimos reusar os nomes definidos em suas
  diretivas. F-strings só funcionam com as variáveis definidas no mesmo escopo, ou seja, do mesmo bloco de
  código para fora.
* F-strings têm mais poder para avaliar expressões arbitrárias de código dentro de suas {chaves}. Por exemplo,
  `f"{2 + 2}"` é válido e retorna `"4"`, mas `"{2 + 2}".format()` espera a chave `2 + 2`.

Por estes motivos, e também porque a tabela para ambas as formas será a mesma, todas as diretivas contarão
com nomes. Para o método `format`, pode-se transformar uma diretiva nomeada em uma posicional ao remover
tal nome: `{nome}` → `{}`. Além disso, para diretivas semelhantes às do estilo C, a descrição será omitida
**desta tabela** a menos que uma informação adicional ou diferente se faça presente. Caso contrário, a descrição
**da tabela acima** vale.

| Diretiva de formatação | Descrição                                                        |          Exemplo com format           |   Resultado    |
|------------------------|------------------------------------------------------------------|:-------------------------------------:|:--------------:|
| {nome}                 | Mesmo que `%(nome)s`.                                            |   `"{ling}".format(ling="Python")`    |   `"Python"`   |
| {nome:10}              |                                                                  |  `"{nome:10}".format(nome="Python")`  | `'    Python'` |
| {nome:<10}             | Mesmo que `%(nome)-10s`.                                         | `"{nome:<10s}".format(nome="Python")` | `'Python    '` |
| {nome:d}               | Mesmo que `%(nome)d`.                                            |   `"{numero:d}".format(numero=100)`   |    `"100"`     |
| {nome:<10d}            |                                                                  | `"{numero:<10d}".format(numero=100)`  | `"100       "` |
| {nome:^10d}            | Formata `nome` e o centraliza em 10 espaços.                     | `"{numero:^10d}".format(numero=100)`  | `"   100    "` |
| {nome:10d}             |                                                                  |  `"{numero:10d}".format(numero=100)`  | `"       100"` |
| {nome:>10d}            | O mesmo que acima.                                               | `"{numero:>10d}".format(numero=100)`  | `"       100"` |
| {nome:010d}            |                                                                  | `"{numero:010d}".format(numero=100)`  | `"0000000100"` |
| {nome:*10d}            | O mesmo que acima, mas preenche espaços com `*` em vez de zeros. | `"{numero:*10d}".format(numero=100)`  | `"*******100"` |
| {nome:+d}              |                                                                  |  `"{numero:+d}".format(numero=100)`   |    `"+100"`    |
| {nome:f}               |                                                                  |  `"{numero:f}".format(numero=3.14)`   |  `"3.140000"`  |
| {nome:6f}              |                                                                  |  `"{numero:6f}".format(numero=3.14)`  |  `"3.140000"`  |
| {nome:6.2f}            |                                                                  | `"{numero:6.2f}".format(numero=3.14)` |   `"  3.14"`   |
| {nome:e}               | Exibe o número no formato científico.                            |     `"{:.2e}".format(0.00000314)`     |  `"3.14e-06"`  |

***

[← aula 6](./aula06.md) | [aula 8 →](./aula08.md)

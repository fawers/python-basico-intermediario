# Parametrização de funções
Aprendemos sobre as várias formas de parametrização de funções nesta aula, fazendo uso de constructos como `*args`,
`**kwargs`, `/`, e `*`.  
Aprendemos que podemos restringir parâmetros para serem passados apenas posicionalmente ou nominalmente, e também
receber um número indeterminado de argumentos posicionais ou nominais.

## Restringindo a passagem de argumentos
Em alguns casos, pode ser interessante que passemos alguns argumentos apenas de forma posicional. Pode ser por
capricho, pode ser para deixar a chamada da função mais legível. Considere a sentença em português, por exemplo:

_Preciso escrever as minhas anotações no meu caderno._

Em pseudo-código derivado de Python, poderíamos escrever desta forma:

    escrever(anotacoes, em=caderno)

E esse poderia ser um bom exemplo de quando forçar a restrição de parâmetros: quando ajudar na leitura.

### Forçando a forma posicional
Usamos a barra `/` junto com os parâmetros da função para forçar que todos à esquerda dela sejam passados de forma
posicional.

```python
def escrever(o_que, /):
    # fazer algo com `o_que`
```

Podemos chamar a função escrevendo `escrever(anotacoes)`, mas não `escrever(o_que=anotacoes)`.

### Forçando a forma nominal
Usamos o asterisco `*` junto com os parâmetros para forçar que todos à direita dele sejam passados de forma nominal.

```python
def escrever(*, em):
    # fazer algo com `em`
```

Podemos chamar a função escrevendo `escrever(em=um_arquivo)`, mas não `escrever(um_arquivo)`.

### Usando ambas as formas
Finalmente, para chamar a função do jeito que idealizamos acima, podemos misturar as duas formas, usando sempre
a barra antes do asterisco.

```python
def escrever(o_que, /, *, em):
    # realizar a escrita

if __name__ == '__main__':
    escrever(minhas_anotacoes, em=um_arquivo_de_texto)
```

## Recebendo um número indeterminado de argumentos
Já vimos funções como `print`, por exemplo, que podem receber um número arbitrário de argumentos. Se abrirmos sua
documentação, veremos que a assinatura da função é algo como:

    print(*values, file=sys.stdout, end='\n', ...)

O que nos interessa aqui é o parâmetro `*values`. Em Python, colocar um asterisco do lado de um parâmetro significa
que ele deverá guardar zero ou mais argumentos durante a sua execução. Internamente, `values` é uma tupla de valores.

```python
def imprimir_varios(*valores):
    print(f"{valores=}")

imprimir_varios("uma string", 2, 3.14, True)
```

Convencionalmente, este parâmtro é chamado de `*args`; usaremos esse nome para nos referirmos a este constructo.

Junto com o `*args`, temos o `**kwargs`, que armazena os argumentos arbitrários em um dicionário, junto com os nomes
associados a eles.

```python
def recebendo_kwargs(**kwargs):
    print(f"{kwargs=}")

recebendo_kwargs(chave='valor', pi=3.14, paciencia=None)
```

## Apêndice: Parâmetros? Argumentos? Nomenclatura
Até então, sempre nos referimos aos parâmetros como "variáveis de entrada" da função, e aos argumentos como "valores
que passamos às funções". A partir deste ponto, usaremos a nomenclatura correta: as variáveis que se encontram na
assinatura das funções (tal como `*valores`, `*args`, `o_que`, `em`) são **parâmetros**. Assim dizemos pois as
funções são _parametrizáveis_, ou seja, podem ser configuradas de acordo com as nossas necessidades. Os valores que
passamos às funções são os **argumentos**. Não importa se passamos os valores diretamente, literalmente, ou por meio
de variáveis. Podemos atribuir `3.14` à variável `pi` e passá-la, ou passar diretamente `3.14`; ambos são
argumentos.

***

[← aula 10](./aula10.md) | [aula 12 →](./aula12.md)

# Python: do Básico ao Intermediário
 
Seja bem-vindo ao curso de Python do Básico ao Intermediário!

Esta apostila foi feita juntamente às aulas gravadas e serve de material de apoio. Não é recomendada a sua
leitura por si só; algumas informações podem parecer ambíguas ou simplesmente fora de contexto. Use a
apostila juntamente com as aulas gravadas!

***

## Conteúdo

| Nome                                                                            |
|---------------------------------------------------------------------------------|
| [Aula 1: Ambientação e primeiros passos](./aula01.md)                           |
| [Aula 2: Tipos de dados e operadores](./aula02.md)                              |
| [Aula 3: Operadores booleanos e tabelas verdade](./aula03.md)                   |
| [Aula 4: Introdução a funções e tomada de decisões](./aula04.md)                |
| [Aula 5: Controle de fluxo com loops](./aula05.md)                              |
| [Aula 6: Introdução a coleções](./aula06.md)                                    |
| [Aula 7: Formatando strings em Python](./aula07.md)                             |
| [Aula 8: Operações de strings](./aula08.md)                                     |
| [Aula 9: Operações de coleções](./aula09.md)                                    |
| [Aula 10: Introdução a tratamento de erros](./aula10.md)                        |
| [Aula 11: Parametrização de funções](./aula11.md)                               |
| [Aula 12: Modularização de código](./aula12.md)                                 |
| [Aula 13: Leitura e escrita de arquivos de texto](./aula13.md)                  |
| [Aula 14: Recursão e Pattern Matching](./aula14.md)                             |
| [Aula 16: Introdução à Orientação a Objetos](./aula16.md)                       |
| [Aula 17: Testes unitários](./aula17.md)                                        |
| [Apêndice: Um olhar voltado ao mercado sobre a linguagem Python](./appendix.md) |

## Links úteis

- [LinkedIn](https://www.linkedin.com/in/fabricio-werneck)
- [Github](https://github.com/fawers)
- [Telegram](https://t.me/+XH9al4e985xkMDVh)

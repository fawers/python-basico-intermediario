# Aula 1: Ambientação e primeiros passos
Nesta aula vimos sobre as ferramentas que podemos usar para programar em Python, e também começamos a dar
os primeiros passos com as operações básicas de entrada e saída.

## A linguagem Python
Tudo o que precisamos saber sobre a linguagem pode ser encontrado [no site oficial dela](https://www.python.org/).
Informações como download das versões para cada plataforma, documentação e definições, tutoriais oficiais
estão todas praticamente estampadas na página inicial do site. Para facilitar, aqui temos listados os links
para download do Python para [Windows](https://www.python.org/downloads/windows/),
[MacOS](https://www.python.org/downloads/macos/), e também [outras plataformas](https://www.python.org/download/other/). 

**Se você está instalando o Python no Windows**, lembre-se de **marcar** a opção `Add Python 3.x to PATH`.
Veja como a opção se parece na imagem a seguir, na porção inferior:

![](https://docs.python.org/pt-br/3/_images/win_installer.png)

## Editores de texto usados no curso
Durante a aula também falamos a respeito de editores de texto que usamos para escrever nossos códigos. No curso gravado,
o editor usado no começo ao fim é o [PyCharm](https://www.jetbrains.com/pycharm/download/) versão Community, da JetBrains.
É um editor de texto completo e voltado completamente ao desenvolvimento com Python. Não é necessária a instalação de
plugins adicionais para o seu funcionamento, então é só instalar e usar. **Tenha certeza de baixar a versão Community,
que é gratuita**. A versão Professional oferece 30 dias gratuitos de uso, e após isso, é necessário pagar por uma
licença. O PyCharm é o editor recomendado caso não haja uma preferência prévia por outro.

Além do PyCharm, falamos também do VS Code e do Replit.  
O [VS Code](https://code.visualstudio.com/) é um editor de texto baseado no poderoso Visual Studio, da Microsoft, tem
seu código [aberto no Github](https://github.com/microsoft/vscode) e suporta um universo de plugins. Caso você opte por
este, eu recomendo a instalação das extensões [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
e [Pylance](https://marketplace.visualstudio.com/items?itemName=ms-python.vscode-pylance). Elas devem ser o suficiente
para acompanhar o curso.

Nossa terceira opção é o [Replit](https://replit.com/). O Replit é a alternativa para quem não quer ou pode instalar um
editor de texto na máquina por limitações de, por exemplo, máquina antiga, ou de uso apenas profissional, em que não se
pode instalar programas sem um bom motivo relacionado ao trabalho. Ele é um editor de texto que eu, particularmente,
considero bastante completo para um editor baseado na nuvem. Mas há algumas limitações; será mais difícil mexer com
funcionalidades como o ambiente virtual do Python. O lado bom dessa limitação é que falamos apenas bastante brevemente
sobre tais ambientes virtuais durante a aula gravada.

## Operações de entrada e saída
Na aula 1 vimos duas operações (duas funções) que usaremos ao longo do curso para entrada e saída: `print`
e `input`.

Usamos `print` para exibir mensagens e dados na tela, e `input` para receber dados do usuário por meio do
teclado. Um exemplo de saudação usando as funções:

```python
name = input("Qual é o seu nome? ")
print(f"Olá, {name}!")
```

Tanto `print` quanto `input` opcionalmente recebem dados e valores para exibir mensagens e, também, _como_
exibí-los. Significa que é possível usá-las da seguinte forma:

```python
variavel = input()
print(variavel)
print()
```

***

[aula 2 →](./aula02.md)

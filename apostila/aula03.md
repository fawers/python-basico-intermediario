# Aula 3: Operadores booleanos e tabelas verdade
Nesta aula continuamos com os operadores da linguagem e seguimos com as tabelas verdade que usamos para
operações booleanas.

## Operadores de comparação
Todos os operadores de comparação, quando aplicados, nos devolvem valores booleanos, ou seja, verdadeiro
(`True`) ou falso (`False`). Com eles conseguimos montar expressões booleanas simples. Juntamente com os
operadores booleanos, conseguimos montar expressões mais complexas para validar condições e regras lógicas
de negócio.

| Operador | Função               | Exemplo  | Resultado |
|:--------:|----------------------|:--------:|-----------|
|   `==`   | Igualdade            |  2 == 2  | True      |
|   `!=`   | Diferença            |  2 != 2  | False     |
|   `>`    | Maior que            |  2 > 2   | False     |
|   `<`    | Menor que            |  2 < 2   | False     |
|   `>=`   | Maior que ou igual a |  2 >= 2  | True      |
|   `<=`   | Menor que ou igual   |  2 <= 2  | True      |
|   `is`   | Identidade           | o1 is o2 | *         |

\* O operador de identidade verifica se duas variáveis se referem ao mesmo valor ("objeto") na memória do
computador. O exemplo `o1 is o2` retorna `True` se forem o mesmo objeto, `False` caso contrário.

```python
o1 = [2]
o2 = [2]
o3 = o1

print(o1, o2, o3)
print(o1 is o2)  # falso - não são o mesmo objeto
print(o1 is o3)  # verdadeiro - são o mesmo objeto

o1[0] += 1  # alteramos apenas o1
print(o1, o2, o3)
# tanto o1 e o3 serão [3] enquanto apenas o2 será [2]
# pois o1 e o3 se referem ao mesmo valor na memória do computador.
# entraremos em mais detalhes na aula sobre coleções e mutabilidade de dados.
```

## Operadores booleanos
Os operadores booleanos são os operadores que nos permitem compor expressões booleanas. Os operandos são
valores booleanos e o resultado da operação também é booleano.

| Operador |     Exemplo      | Resultado |
|:--------:|:----------------:|-----------|
|  `and`   | `True and False` | `False`   |
|   `or`   | `True or False`  | `True`    |
|  `not`   |    `not True`    | `False`   |

## Tabelas verdade
Usamos as tabelas verdade como referência de uso dos operadores booleanos a fim de compor expressões
booleanas complexas. Não é necessário decorar a tabela inteira, mas é relevante saber quando uma expressão
`and` é verdadeira e uma `or` é falsa.

### AND (E)
| `p` = idade >= 16 | `q` = possui titulo | `p and q` = pode votar |
|:-----------------:|:-------------------:|:----------------------:|
|    Verdadeiro     |     Verdadeiro      |       Verdadeiro       |
|    Verdadeiro     |        Falso        |         Falso          |
|       Falso       |     Verdadeiro      |         Falso          |
|       Falso       |        Falso        |         Falso          |

Lê-se: Eu só posso votar se tiver 16 anos ou mais, e possuir um título de eleitor.

### OR (Ou)
| `p` = caminha na rua | `q` = caminha na esteira | `p or q` = se exercita |
|:--------------------:|:------------------------:|:----------------------:|
|      Verdadeiro      |        Verdadeiro        |       Verdadeiro       |
|      Verdadeiro      |          Falso           |       Verdadeiro       |
|        Falso         |        Verdadeiro        |       Verdadeiro       |
|        Falso         |          Falso           |         Falso          |

Lê-se: Eu só não me exercito quando não caminho nem na rua e nem na esteira.

### NOT (Negação)
| `p` = eu sou maior de idade |              `not p`               |
|:---------------------------:|:----------------------------------:|
|         Verdadeiro          |               Falso                |
|            Falso            |             Verdadeiro             |

***

[← aula 2](./aula02.md) | [aula 4 →](./aula04.md)

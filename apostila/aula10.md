# Introdução a tratamento de erros
Nesta aula, conhecemos a estrutura `try/except`, que nos ajuda a capturar e tratar possíveis erros.

Considere o programa:

```python
idade = int(input("Digite sua idade: "))
```

Ele é problemático pois a conversão de string (retornada, neste caso, pela função `input`) para inteiro
pode ocasionar em um erro de valor se a string não for um inteiro válido. Nós não queremos que nossos
usuários vejam erros não tratados em nossos programas. Por isso dizemos ao Python para _tentar_ executar
um pedaço de código. Caso uma _exceção_, ou erro, aconteça, o capturamos com a instrução `except`.

```python
try:
    idade = int(input("Digite sua idade: "))
except:
    print("Sua idade deve ser um número.")
```

Ainda podemos especificar qual erro queremos tratar ao escrever seu nome ao lado de `except`:

```python
try:
    idade = int(input("Digite sua idade: "))
except ValueError:
    print("Sua idade deve ser um número.")
```

***

[← aula 9](./aula09.md) | [aula 11 →](./aula11.md)

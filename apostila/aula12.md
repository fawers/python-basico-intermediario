# Modularização de código
Nesta aula vimos como modularizar o nosso código e por que devemos nos preocupar com a legibilidade da nossa base
de código.

Aprendemos que podemos reusar módulos existentes usando a instrução `import`, seguido do nome do módulo. Aprendemos
também que um módulo nada mais é do que um arquivo de código-fonte do Python com a extensão `.py`.

```python
import sys
```

Ao realizarmos a importação do módulo, podemos usar qualquer entidade - funções, tipos, constantes - ao prefixá-la
com o nome do módulo como, por exemplo,

```python
# arquivo: aula12.py
import sys

print(sys.argv)
```

Ao executar este programa passando diferentes argumentos de linha de comando, podemos ver que eles aparecem na lista
`argv` contida no módulo `sys`.

```shell
$ python aula12.py 
['aula12.py']
$ python aula12.py arg1
['aula12.py', 'arg1']
$ python aula12.py arg1 arg2 arg3
['aula12.py', 'arg1', 'arg2', 'arg3']
$ python aula12.py arg1 --arg2=True -a3
['aula12.py', 'arg1', '--arg2=True', '-a3']
$ python aula12.py args "com espaços"
['aula12.py', 'args', 'com espaços']
```

Outras formas de importar módulos incluem:

```python
import sys  # comum
sys.argv
sys.modules

import sys as s  # apelidando o módulo
s.argv
s.modules

from sys import argv, modules  # importando entidades específicas
argv
modules

from sys import *  # importando tudo do módulo
argv
modules
# ^^^ esta forma não é recomendada. se vc deseja importar tudo,
# use a primeira ou segunda forma.
```

Também aprendemos que o arquivo pelo qual iniciamos nosso programa recebe um nome especial: `__main__`.

Por padrão, o nome de um módulo é o nome do seu arquivo, incluindo todas as pastas até chegar nele. Se tivermos a
seguinte estrutura de pastas:

```
app
+-- pacote
    +-- modulo1.py
    +-- modulo2.py
+-- modulo3.py
main.py
```

E iniciarmos nosso programa pelo `main.py`, conseguiremos importar o módulo 2 escrevendo:

```python
import app.pacote.modulo2
# ou
import app.pacote.modulo2 as m2
# ou
from app.pacote import modulo2
```

E a propriedade `__name__` do módulo (`modulo2.__name__`) será `app.pacote.modulo2`. Mas o nome de `main.py` não é
`main` (exatamente igual ao nome do arquivo), e sim `__main__` (com underscores, um nome especial do Python).

## PIP e VirtualEnv
Nesta aula nós também aprendemos brevemente sobre ambientes virtuais e como baixar pacotes de terceiros via pip.

O ambiente virtual serve para isolar as dependências de um projeto do resto do ambiente do Python. É comum que haja
um ambiente virtual para cada projeto Python, pois assim eles podem ter versões diferentes de um mesmo pacote, ou
até mesmo pacotes conflitantes entre si. O conflito de versão ou de pacotes não acontece justamente porque são
ambientes isolados uns dos outros.

Para criar um ambiente virtual, abra um terminal (Prompt de Comando ou PowerShell no Windows) e digite o seguinte
comando:

```
python  -m venv --prompt "nome do projeto" pasta_do_ambiente
#~~~~~^ use python3 se for necessário no seu sistema. 
```

Substitua o `"nome do projeto"` com o nome do seu projeto, como por exemplo, `"discografia"` e `pasta_do_ambiente`
pelo nome da pasta onde o ambiente será instalado. Esta pasta é comumente chamada de `.venv`.

Para ativar um ambiente virtual, execute no terminal (na mesma pasta onde o comando anterior foi executado):

```
# Unix (Linux, MacOS):
. .venv/bin/activate

# Windows:
.venv\Scripts\activate.ps1  # ou .bat
```

O terminal deve passar a mostrar o argumento que passamos a `--prompt` acima entre parênteses. 

***

Para baixar pacotes de terceiros, usamos a ferramenta `pip`. O pip já vem instalado nas versões mais recentes
do Python, então você já deve tê-lo instalado no sistema.

Com o ambiente virtual ativado, execute no terminal:

```
pip install requests
# ou
python -m pip install requests
```

`requests` é uma biblioteca escrita para facilitar a criação e uso de requisições HTTP. Podemos usufruir de diversos
serviços hospedados na web por meio dessa biblioteca.

Várias outras bibliotecas e pacotes podem ser baixados diretamente do [Python Package Index](https://pypi.org/search/).

***

[← aula 11](./aula11.md) | [aula 13 →](./aula13.md)

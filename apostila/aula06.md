# Introdução a coleções
Na aula 6 começamos a estudar as coleções, ou como eu gosto de chamar, os tipos contêineres. As coleções
são valores que podem conter outros valores, como listas e conjuntos. Uma lista por si só não tem muito a
oferecer; o seu forte é poder armazenar internamente um ou mais valores de um tipo para que possamos, entre
outras coisas, percorrê-los com um loop `for`.

## Listas
Listas normalmente são a primeira coleção que aprendemos quando estudamos programação. As listas armazenam
elementos de maneira sequencial, um atrás do outro. Podemos acessar esses elementos por meio de um índice, que
começa a partir do zero. Além disso, listas possuem métodos (operações) para adicionar elementos, removê-los,
ordená-los, entre outros.

```python
# criando uma lista vazia
l = []
# ou com elementos
l = ['zero', 'um', 'três']

# adicionando ítens
# ao final da lista
l.append('quatro')
# no índice especificado
l.insert(2, 'dois')

# adicionando os elementos de outra lista ao final desta
l.extend(['cinco', 'seis', 'sete'])

# acessando o primeiro elemento
print(l[0])
# o segundo
print(l[1])
# o último
print(l[-1])

# removendo o elemento 'zero'
l.remove('zero')

# removendo o último elemento
popado = l.pop()  # pop() retorna o elemento removido
# removendo o elemento do meio
tamanho = len(l)  # len = length = comprimento, tamanho
popado = l.pop(tamanho // 2)
```

## Tuplas
No Python, as tuplas são basicamente listas imutáveis, ou seja, não conseguimos alterá-la (adicionar ou
remover elementos, etc) como conseguimos com listas. Tuplas podem ser usadas para retornar mais do que um
valor de funções, ou agrupar valores em listas ou dicionários, entre outros.

```python
# criando uma tupla vazia
t = ()
# com dois ou mais elementos
t = ('a', 'b')
# com apenas um elemento
t = ('z',)  # <-- note a vírgula

# criando uma terceira tupla a partir de duas,
# adicionando os elementos da segunda tupla ao final
a = ('a', 'b', 'c')
b = ('d', 'e', 'f')
c = a + b
print(f"{a=}, {b=}, {c=}")

# retornando uma tupla de uma função
def imc(peso, altura):
    return (25, 'Peso normal')
```

## Dicionários
Dicionários são como listas no sentido que ambos são indexáveis, alteráveis (podemos adicionar ou remover
elementos), percorríveis com loops `for`. Eles são diferentes na forma de indexação - dicionários utilizam
'chaves', que normalmente são strings - e armazenamento interno - listas são sequenciais, enquanto dicionários
são o que chamamos de HashMap. Usamos dicionários quando queremos descrever o que um certo valor é pela sua
chave.

```python
# criando um dicionário vazio
d = {}
# com um ou mais itens
d = {'linguagem': 'Python'}

# adicionando elementos
d['nivel'] = 'basico-intermediario'
d.update(duracao='x', professor='Fabricio')

# removendo elementos
popado = d.pop('duracao')
```

## Conjuntos
Por fim, temos os conjuntos. Conjuntos em Python são os filhos das listas com dicionários: podemos alterá-los
e o seu método de indexação é, semelhantemente aos dicionários, por hashes. Mas conjuntos têm o seu próprio
DNA: não podem ter elementos duplicados e têm suas próprias operações que se assemelham ao conjunto
matemático, como intersecção, união, diferença, etc.

```python
# criando conjuntos
multiplos_de_2 = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20}
multiplos_de_3 = {3, 6, 9, 12, 15, 18, 21}

# aplicando intersecção para obter os múltiplos de 2 e de 3 ao mesmo tempo
multiplos_de_6 = multiplos_de_2 & multiplos_de_3

# aplicando união para obter todos os múltiplos em um só conjunto
multiplos_de_2_e_3 = multiplos_de_2 | multiplos_de_3

# aplicando diferença para obter os múltiplos de 3 que não são múltiplos de 2
múltiplos_de_3_impares = multiplos_de_3 - multiplos_de_2
```

![conjuntos e suas operações](./img/aula6/img00.png)

***

[← aula 5](./aula05.md) | [aula 7 →](./aula07.md)

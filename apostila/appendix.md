# Um olhar voltado ao mercado sobre a linguagem Python

A linguagem Python é uma linguagem de programação de alto nível usada nas mais variadas áreas dentro da TI, como
desenvolvimento web, computação científica, análise de dados, inteligência artificial, entre outras. Ela emergiu em 1991
com o propósito de facilitar a integração entre alguns sistemas escritos em C, e desde então vem sido largamente usada e
se tornou uma das linguagens de programação mais populares ao redor do mundo.

O primeiro atrativo que as pessoas percebem na linguagem é a sua simplicidade e facilidade de rapidamente entender o
código. A linguagem foi concebida com a ideia de ser legível e de fácil uso justamente para alavancar a produtividade de
seus usuários, visto que as particularidades do C foram abstraídas e escondidas por trás de uma biblioteca padrão bastante
completa. Diz-se que a linguagem Python vem "com baterias inclusas" por conta disso. Adicionalmente, existe uma gigante
comunidade mundial bem ativa de programadores Python, o que resulta em vários frameworks e bibliotecas desenvolvidos por
terceiros e uma alta probabilidade de se conseguir recursos para aprender ou resolver problemas pontuais.

Sua biblioteca versátil inclui módulos extensivamente testados para operar com a web, por meio de requisições e
respostas HTTP, ou para manipular dados, com ferramentas de manipulação de texto e dados binários em geral, entre várias
outras coisas. Essa biblioteca completa permite que programadores autônomos construam ferramentas ainda mais completas e
complexas em cima dela. Por exemplo, é possível usar as ferramentas de manipulação binária para escrever uma biblioteca
que manipula arquivos de imagens PNG.

A implementação padrão do Python faz dela uma linguagem interpretada. Diferentemente das linguagens compiladas, como C,
linguagens interpretadas oferecem uma praticidade maior no momento de testar código e prototipagens. Isso também facilita
o uso de terminais e notebooks interativos, como o próprio shell do Python, ou notebooks Jupyter.

Então, pontuando algumas das vantagens da linguagem Python:

- **Largamente usada ao redor do globo.** Dificilmente você encontrará algum programador que nunca escreveu uma linha de
  Python. A linguagem é poderosa e popular.
- **Fácil de aprender.** Existem alguns códigos Python que lemos como se estivessemos lendo sentenças em inglês.
- **Versátil.** De desenvolvimento web a aprendizado de máquina a inteligência artificial a scripting em sistemas Unix.
- **Vasta comunidade.** Uma comunidade global e ativa que está pronta para auxiliar no que for necessário.

Mas claro, nem tudo é um mar de rosas. A linguagem Python, assim como qualquer outra, tem os seus lados negativos,
também. Se algum desses próximos pontos for de suma importância, talvez seja melhor reconsiderar a escolha da linguagem
de programação para aprender.

- **Performance.**

  A implementação padrão do Python é, como mencionada acima, interpretada e não compilada. Isso significa que o código
  Python é lido e validado a cada vez que o executamos. Com linguagens compiladas, como C ou Rust, o compilador lê e
  valida o código apenas uma vez antes de transformá-lo em código binário que a máquina entende diretamente. Python ser
  interpretado nos dá a dinamicidade e praticidade de poder usar shells e notebooks pelo preço de não ser, nem de longe,
  a linguagem mais rápida. Claro, ela ainda é rápida como um raio aos olhos humanos. Mas se você precisa de performance
  com precisão de milisegundos, ou até microsegundos, Python pode não ser a melhor escolha.

- **Menos controle sobre a memória do computador.**

  O Python usa algo chamado _garbage collector_, ou "coletor de lixo". Por causa dele nunca precisamos nos preocupar em
  como gerenciar a memória do computador. Alocação, realocação ou liberação de memória não é algo que existe na superfície
  da linguagem. O próprio Python cuida de alocar memória quando necessário, e o coletor de lixo a libera quando não é mais
  necessária. O lado ruim disso é que a execução do coletor de lixo pode acontecer simultaneamente com o nosso programa,
  afetando drasticamente a sua performance. Com isso são dois pontos que mencionam a performance do Python.

- **Sintaxe.**

  Algumas pessoas podem dizer que a sintaxe do Python é menos expressiva, menos conveniente do que outras linguagens.
  Para as ocasiões em que isso importa, a linguagem pode ser menos atraente.

- **Desenvolvimento mobile, de jogos, e de sistemas de baixo nível.**

  Python não é bom para desenvolvimento de aplicativos móveis para Android ou iOS, ponto final. Existe um framework
  chamado Kivy, que traz um ecossistema para desenvolvimento mobile em Python, mas ele é abaixo da média. Prefira as
  nativas Kotlin ou Swift para desenvolvimento Android ou iOS, respectivamente.

  O mesmo vale para o desenvolvimento de jogos. Ainda que haja bibliotecas e frameworks para desenvolver jogos em Python,
  como pygame, a linguagem raramente é a preferida para esse papel. Melhor se ater aos grandes Unity, Unreal, e Godot para
  jogos.

  Finalmente, o Python também não possui um bom suporte para desenvolvimento de sistemas de baixo nível. Normalmente esses
  sistemas precisam ser responsivos e performáticos, então linguagens como C, C++ ou Rust são usadas para eles. Python
  nem pensar.

Para concluir, é sempre importante levar em conta qual é o propósito do projeto e quais as melhores ferramentas e
tecnologias para desenvolvê-lo. Hoje em dia, para a grande maioria das tarefas, o Python é uma ótima escolha e, por ser
de fácil aprendizado, escrever código nele será efetivo e eficiente.

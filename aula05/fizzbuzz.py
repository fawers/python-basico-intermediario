def fizzbuzz():
    contador = 1

    while contador <= 102:
        if contador % 3 == 0:  # se for divisível por 3
            print(f"Fizz{'Buzz' if contador % 5 == 0 else ''}")

        elif contador % 5 == 0:
            print("Buzz")

        else:
            print(contador)

        contador += 1


def main():
    print("Loop do FizzBuzz!")
    print("Contar de 1 a 100")
    print("Imprimir o número")
    print("ou 'Fizz' se for múltiplo de 3,")
    print("'Buzz' se for múltiplo de 5\n")
    fizzbuzz()


if __name__ == '__main__':
    main()

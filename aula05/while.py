# while - enquanto

def while_condicao(contador=10):
    # enquanto o valor de contador for maior do que zero
    while contador > 0:
        print(contador)
        contador -= 1  # contador = contador - 1

    print("ignição!")


def while_true():
    while True:
        print("loop infinito! não consigo sair!")


def main():
    while_true()
    

if __name__ == '__main__':
    main()

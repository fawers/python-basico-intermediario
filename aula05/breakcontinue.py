def main():
    contador = 20

    while True:
        if contador % 2 == 0:
            contador -= 1
            continue

        print(contador)

        if contador == 5:
            break

        contador -= 1

    print("pós loop")


if __name__ == '__main__':
    main()

def for_range():
    # range - intervalo numérico
    # sequência de números
    # para cada I no intervalo [10..0[
    for i in range(10, 0, -1):
        if i == 5:
            continue

        print(i)

        if i == 2:
            break


def for_seq():
    seq = ['Fabricio', 'Python', 'pycharm', 'loop in seq']

    # para cada string nesta sequencia de strings
    for string in seq:
        print(string)


def for_str():
    # para cada caractere C na string Fabricio
    for c in 'Fabricio':
        print(c)


def main():
    for_str()


if __name__ == '__main__':
    main()

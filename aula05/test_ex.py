from io import StringIO
import unittest
from unittest.mock import patch

import ex


class TestEx(unittest.TestCase):
    def test_tabuada(self):
        for i, e in [(7, "7\n14\n21\n28\n35\n42\n49\n56\n63\n70\n"),
                     (12, '12\n24\n36\n48\n60\n72\n84\n96\n108\n120\n'),
                     (-3, '-3\n-6\n-9\n-12\n-15\n-18\n-21\n-24\n-27\n-30\n'),
                     (0, '0\n0\n0\n0\n0\n0\n0\n0\n0\n0\n')]:
            with self.subTest(entrada=i, esperado=e):
                with patch('sys.stdout', StringIO()) as out:
                    ex.tabuada(i)
                    out.seek(0)
                    self.assertEqual(out.read(), e)

    def test_inverter_numero(self):
        for i, e in [(12345, 54321),
                     (12321, 12321),
                     (159, 951),
                     (3210, 123)]:
            with self.subTest(entrada=i, esperado=e):
                self.assertEqual(ex.inverter_numero(i), e)


if __name__ == '__main__':
    unittest.main()
